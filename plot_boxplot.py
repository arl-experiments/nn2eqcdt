import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# These are computation times from the 30 runs of the NN2EQCDT algo on the simple model for MCC env

comp_times = [9.697500705718994, 9.24909257888794, 10.044373035430908, 10.589450597763062, 9.666483402252197,
              9.740437030792236, 9.881762027740479, 9.440764665603638, 9.499186038970947, 9.697527885437012,
              9.87828278541565,
              10.434370279312134, 10.627682209014893, 11.264201879501343, 10.537338972091675, 10.318344593048096,
              10.127582788467407, 10.31631064414978, 9.754087448120117, 9.457444190979004, 9.181540250778198,
              9.294447422027588, 9.45708155632019, 9.776527166366577, 9.002622604370117, 9.269506216049194,
              10.506573677062988,
              9.116448640823364, 9.990630388259888, 9.054943799972534]


def get_box_plot_data(labels, bp):
    rows_list = []

    for i in range(len(labels)):
        dict1 = {}
        dict1['label'] = labels[i]
        dict1['lower_whisker'] = bp['whiskers'][i * 2].get_ydata()[1]
        dict1['lower_quartile'] = bp['boxes'][i].get_ydata()[1]
        dict1['median'] = bp['medians'][i].get_ydata()[1]
        dict1['upper_quartile'] = bp['boxes'][i].get_ydata()[2]
        dict1['upper_whisker'] = bp['whiskers'][(i * 2) + 1].get_ydata()[1]
        rows_list.append(dict1)

    return pd.DataFrame(rows_list)


labels = ['comp_times']
bp = plt.boxplot([np.array(comp_times)], labels=labels)
print(get_box_plot_data(labels, bp).to_string())
plt.show()

# output:
#        label  lower_whisker  lower_quartile    median  upper_quartile  upper_whisker
# 0  comp_times       9.002623        9.444844  9.747262       10.269129      11.264202
