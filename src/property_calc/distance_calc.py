from typing import List

import numpy as np
from numpy import dot
from numpy.linalg import norm
from src.models.graph_structures import Expr


def calc_neighbor_distances(expr_node1, expr_node2):
    node1_expr: List[Expr] = expr_node1.value
    node2_expr: List[Expr] = expr_node2.value

    assert len(node1_expr) == len(node2_expr)

    cos_distances = []

    for i in range(len(node1_expr)):
        cos_distance = _calc_cosine_distance(node1_expr[i].normal_vec, node2_expr[i].normal_vec)
        cos_distances.append(float(cos_distance))
    return cos_distances


def _calc_cosine_distance(x: np.array, y: np.array):
    # Source: https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.cosine.html
    # return distance.cosine(x, y)
    # Source: https://stackoverflow.com/a/18424933
    cos_sim = dot(x, y) / (norm(x) * norm(y))
    return 1 - cos_sim
