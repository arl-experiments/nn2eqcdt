def eval_node_expression(T, x, y, current_node):
    import numexpr as ne
    sat_node_expr = T.nodes[current_node]["value"]
    eval_value = ne.evaluate(sat_node_expr)
    return float(eval_value), sat_node_expr


def calc_output(T, x_list, y_list, current_node):
    z_list = []
    sat_node_expr_list = []
    for x, y in zip(x_list, y_list):
        z, chosen_sat_node = eval_node_expression(T, x, y, current_node)
        z_list.append(z)
        sat_node_expr_list.append(chosen_sat_node)

    return z_list, sat_node_expr_list


def calc_data_points(T, x, y, sat_nodes):
    import json

    x_list, y_list, z_list, sat_node_expr_list = [], [], [], []

    for idx, (x, y) in enumerate(zip(x[0], y[0])):
        x = [i for i in x if i is not None]
        y = [i for i in y if i is not None]
        z, chosen_sat_node_expr = calc_output(T, x, y, sat_nodes[idx])
        z = [i for i in z if i is not None]

        if len(x) > 0 and len(y) > 0 and len(z) > 0:
            x_list.append(list(x))
            y_list.append(list(y))
            z_list.append(list(z))
            sat_node_expr_list.append(list(chosen_sat_node_expr))

    return x_list, y_list, z_list, sat_node_expr_list


def calc_output_ap(T, ap):
    from src.output_methods import get_output
    ap_output = {'x': [], 'y': [], 'z': [], 'output_node': [], 'sat_node_expr': [], 'r': []}
    for ap_x, ap_y, ap_r in zip(ap['x'], ap['y'], ap['r']):
        ap_output_z, output_node, sat_node_expr = get_output(T, [ap_x, ap_y])
        ap_output['x'].append(ap_x)
        ap_output['y'].append(ap_y)
        ap_output['z'].append(ap_output_z)
        ap_output['output_node'].append(output_node)
        ap_output['sat_node_expr'].append(sat_node_expr)
        ap_output['r'].append(ap_r)
    return ap_output
