from src.graph_methods.access_graph_methods import *
from src.graph_methods.check_graph_methods import *
from src.z3_methods import *


def remove_path_nodes(T, root_node):
    remove_nodes = [v for u, v in nx.bfs_edges(T, root_node)]
    remove_nodes.append(root_node)
    T.remove_nodes_from(remove_nodes)


def convert_final_rule_to_expr(T):
    root_sat_paths = get_root_to_sat_paths(T)
    print("paths for region plotting", root_sat_paths)
    for root_sat_path in root_sat_paths:
        # This gets the last node before a final SAT child node
        parent_sat = root_sat_path[-1][-1]
        # Remove its child nodes (UNSAT and SAT) and corresponding edges to parent
        child_nodes = get_child_nodes(T, parent_sat)
        T.remove_edge(parent_sat, child_nodes[0])
        T.remove_edge(parent_sat, child_nodes[1])
        T.remove_nodes_from(child_nodes)

        # Parent is now final node that is satisfiable of its respective path from the root
        T.nodes[parent_sat]["final_sat"] = True

        # Convert final node rule to expression (replace is not inplace)
        parent_sat_expr = T.nodes[parent_sat]["value"].replace(">0", "")
        T.nodes[parent_sat]["value"] = parent_sat_expr


def remove_unsat_paths(T):
    unsat_nodes = get_unsat_nodes(T)
    print("UNSAT nodes", unsat_nodes)
    for unsat_node in unsat_nodes:
        parent_node = get_parent_node(T, unsat_node)
        sibling_node = get_sibling_node(T, unsat_node)

        # Only reconnect parent_parent_node to non-UNSAT child and remove old connections,
        # if the parent_node is not the root node
        if has_node_parent_node(T, parent_node):
            parent_parent_node = get_parent_node(T, parent_node)
            old_weight_to_parent = T.edges[(parent_parent_node, parent_node)]["weight"]
            T.add_edge(parent_parent_node, sibling_node, weight=old_weight_to_parent)

            T.remove_edge(parent_parent_node, parent_node)

        # This has to be executed if parent_node is root or not root node
        T.remove_edge(parent_node, unsat_node)
        T.remove_edge(parent_node, sibling_node)

        T.remove_nodes_from([parent_node, unsat_node])

