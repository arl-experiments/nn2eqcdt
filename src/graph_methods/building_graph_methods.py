from typing import List

import torch.nn

from src.calc_methods import *
from src.graph_methods.change_graph_methods import *
from src.graph_methods.access_graph_methods import *
from src.models.PathCheckingOptions import PathCheckingOptions
from src.models.graph_structures.Node import Node, NodeKind
from src.models.graph_structures.Path import Path


def build_tree(model, path_checking_options=None):
    T = nx.balanced_tree(2, 0)
    T.graph['path_checking_options'] = path_checking_options

    w_hat, next_node, subgraph_roots_weights_assertions, next_subgraph_roots_weights_assertions = None, None, {}, {}

    # Modified from source: https://stackoverflow.com/questions/54203451/how-to-iterate-over-layers-in-pytorch
    for i, (name, m) in enumerate(model.named_children()):
        if isinstance(m, torch.nn.Linear):
            wt = m.weight.data
            if i == 0:
                w_hat = m.weight.data
                if m.bias is None:
                    bt = torch.zeros(wt.shape[0])
                else:
                    bt = m.bias.data
                b_hat = bt
                rules = calc_rules(w_hat, b_hat)
                # The DT is initialized with the root node containing the first "basic" rule
                rule = rules[0]
                rule.set_rule_to_tree_node(T, 0)
                above_path = Path(T, [])
                node = Node(T, 0, above_path, NodeKind.SAT, rule)

                node.add_to_tree(0)

                T.graph['node_counter'] = 1

                # Because the first rule was already set, the further nodes to be connected should only be gotten from
                # subsequent rules in the list
                rules = rules[1:]
                T.graph['rules'] = rules

                # BUT, because the assertion terms describes the edges, also the first one has to be passed for checking
                # the path TO the current node (AND subsequent ones to added for checking edges FROM the node)

                add_new_subgraph(T, node)
                set_hat_on_SAT_nodes(T, subgraph_roots_weights_assertions, w_hat, b_hat)
                # node -= 1
                print("subgraph_roots_weights_assertions", subgraph_roots_weights_assertions)
            else:
                # The next_subgraph_roots_weights_assertions needs to be cleared,
                # because it is only temporarily used within each for loop iteration to ONLY
                # hold the new endpoint nodes and its corresponding information
                next_subgraph_roots_weights_assertions = {}
                for next_subgraph_root in list(subgraph_roots_weights_assertions.keys()):
                    above_path_assertions = list(
                        subgraph_roots_weights_assertions[next_subgraph_root]['all_path_assertions'])
                    above_path_weights = list(subgraph_roots_weights_assertions[next_subgraph_root]['all_path_weights'])
                    a = compute_a(above_path_weights)
                    # w_hat has to be saved for each path
                    w_hat, b_hat = get_last_w_b_hat_bt(T, next_subgraph_root, m)

                    if m.bias is None:
                        bt = None
                    else:
                        bt = m.bias.data

                    next_w_hat, next_b_hat, bt = calc_new_hat(w_hat, wt, a, b_hat, bt)

                    rules = calc_rules(next_w_hat, next_b_hat, False)
                    rules[0].set_rule_to_tree_node(T, next_subgraph_root)

                    path = Path(T, )

                    rules = rules[1:]
                    next_node, next_subgraph_roots_weights_assertions_tmp = \
                        add_new_subgraph(T, next_subgraph_root, rules,
                                         above_path_assertions,
                                         # The weights are only collected for a subgraph FOR EACH layer not all along a path
                                         # from the overall graph root, because the w_hats are also updated incrementally
                                         # with the weights of a path (a vector,
                                         # see Neural Networks are Decision Tree paper) layer-wise
                                         [],  # NOT above_path_weights,
                                         # This also needs to be empty here, because within each for loop iteration ONLY
                                         # the new endpoints for this iteration
                                         # are collected in next_subgraph_roots_weights_assertions_tmp
                                         # and for them the w_hat is set in the set_w_hat_on_SAT_nodes, than
                                         # the outer next_subgraph_roots_weights_assertions is updated for the overall
                                         # layer endpoints which is than iterated over in the next loop
                                         #
                                         {},  # NOT next_subgraph_roots_weights_assertions,
                                         next_node,
                                         path_checking_options)
                    set_hat_on_SAT_nodes(T, next_subgraph_roots_weights_assertions_tmp, next_w_hat, next_b_hat)
                    next_subgraph_roots_weights_assertions.update(next_subgraph_roots_weights_assertions_tmp)
                    print(next_subgraph_roots_weights_assertions)
                    # Intersection set MUST BE empty
                    # next_subgraph_roots_weights_assertions.update(next_subgraph_roots_weights_assertions)
                intersect_node_list = list(
                    set(next_subgraph_roots_weights_assertions.keys()) & set(subgraph_roots_weights_assertions.keys()))
                if len(intersect_node_list) > 0:
                    raise Exception("The following new endpoint nodes were already endpoint nodes", intersect_node_list)
                subgraph_roots_weights_assertions = copy.deepcopy(next_subgraph_roots_weights_assertions)
                print("next_subgraph_roots_weights_assertions", next_subgraph_roots_weights_assertions)
    return T


def add_node_edge(T, root_node: Node, next_node: Node, weight, rule: Rule,
                  path_checking_options):
    above_root_path: Path = root_node.above_path
    if above_root_path is None:
        raise Exception("Above root path is None")

    all_path_assertions = above_root_path.get_assertions_to_be_checked(weight, rule)
    check_path_res, model = check_path(all_path_assertions, path_checking_options)

    next_node.set_sat_kind(check_path_res, model)

    # All path weights are collected by each add_nodes_edges method
    # by adding the current weight to the above path weights
    # These are then referenced by the root_subgraph
    # Store weight as plain pyton single-number array so that T is json serializable

    # current_path_weight = [weight]  # NOT torch.Tensor([weight])
    # all_path_weights = above_path_weights + [current_path_weight]

    next_node.set_on_child_layer_of(root_node)

    T.add_node(next_node, node=next_node)

    T.add_edge(root_node, next_node, weight=weight, assertion=assertion)
    return check_path_res


def add_new_subgraph(T, root_node):
    """
    This build a new decision subgraph down from the provided subgraph root
    with the rules and corresponding assertion_terms extracted from a w_hat of a matrix layer

    On a root node there are attached two nodes with respecting edges as the lower equal and higher splits
    of the assertions
    by the add_nodes_edges method.
    In each split the method calls itself recursively to add also these nodes on decision splits,
    if the respective path is valid,
    so if all above assertions are satisfiable with the input constraints
    by z3 solver in check_path called in add_nodes_edges method.

    # Assumption: The root MUST already be satisfied with above assertions and be created
    # with its respective rule and corresponding assertion_term
    # as well as above path assertions and weights
    # The node id has to be passed here, because it needs to be increased between two subgraphs of the same layer
    # within the loop over the next_subgraph_roots_a
    # There are two subgraphs created for each decision split for lower equal and higher in add_nodes_edges method.
    """
    if root_node != NodeKind.SAT:
        raise Exception("Root node for new subgraph to add to is not SAT")

    for weight in range(2):
        node_id = get_increase_node_id(T)

        next_node = Node(T, node_id + 1, None)
        if len(rules) > 0:

            check_path_res, all_path_weights, all_path_assertions = add_node_edge(T, root_node, node_id,
                                                                                  weight, rules[0],
                                                                                  path_checking_options)

            if check_path_res == sat:
                node_id = add_new_subgraph(T, node_id, rules[1:], node_id, path_checking_options)

        else:
            # If the rule and assertion term arrays are empty,
            # one subgraph leaf is reached, hence all above path assertions are already checked as SAT,
            # so it only needs to create a new subgraph root node, which will be initially filled outside this function,
            # but be given as root_subgraph in the next initial call this method

            check_path_res, all_path_weights, all_path_assertions = add_nodes_edges(T, root_node_id, node_id,
                                                                                    list(above_path_assertions),
                                                                                    list(above_path_weights),
                                                                                    False,
                                                                                    weight, 'UN_SAT_NODE',
                                                                                    path_checking_options)
            # The all path weights to a SAT-node are collected in the add_nodes_edges method and
            # are referenced to the root of the new subgraph (here SAT node) in order to create the a-vector,
            # expand it and apply it the W matrix and calc the new w_hat in the calc_new_w_hat method

            # only collect SAT nodes
            if check_path_res == sat:
                if node in next_subgraph_roots_a.keys():
                    raise Exception("Node ", node, " already set as next subgraph root")
                next_subgraph_roots_a[node] = {'all_path_weights': list(all_path_weights),
                                               'all_path_assertions': list(all_path_assertions)}

    return node_id


def set_hat_on_SAT_nodes(T, subgraph_roots_weights_assertions, w_hat, b_hat):
    for next_subgraph_root in list(subgraph_roots_weights_assertions.keys()):
        # Convert tensor to python array so that T is json serializable
        T.nodes[next_subgraph_root]['last_w_hat'] = w_hat.numpy().tolist()
        subgraph_roots_weights_assertions[next_subgraph_root]['last_w_hat'] = w_hat

        T.nodes[next_subgraph_root]['last_b_hat'] = b_hat.numpy().tolist()
        subgraph_roots_weights_assertions[next_subgraph_root]['last_b_hat'] = b_hat


def get_increase_node_id(T) -> int:
    node_id: int = T.graph['node_counter']
    T.graph['node_counter'] += 1
    return node_id
