from src.graph_methods.access_graph_methods import *


def is_grand_child_sat_node(T, node):
    """
    This method determins if a grandchld is a SAT node
    because than it cannot be simplified, because the child node
    is "really" the last node, but which has a rule atm
    """
    from src.graph_methods.access_graph_methods import get_child_nodes
    child_nodes = get_child_nodes(T, node)
    grand_child_is_sat_node = False
    if len(child_nodes) == 0:
        raise Exception("No child nodes while checking if grand child is SAT node")
    else:
        for child in child_nodes:
            if T.nodes[child]["value"].startswith("SAT"):
                grand_child_is_sat_node = True
    return grand_child_is_sat_node


def is_child_final_sat_node(T, node):
    """
    This method determins if a chld is a final node that is satisfiable
    """
    from src.graph_methods.access_graph_methods import get_child_nodes

    child_nodes = get_child_nodes(T, node)
    if len(child_nodes) == 0:
        raise Exception("No child nodes found while checking if child is final sat node")
    else:
        for child in child_nodes:
            if is_final_sat_node(T, child):
                return True
        return False


def is_final_sat_node(T, node):
    if "final_sat" in T.nodes[node] and T.nodes[node]["final_sat"]:
        return True
    return False
