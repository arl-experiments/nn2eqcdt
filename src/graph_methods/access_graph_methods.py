import networkx as nx
import torch
from src.graph_methods.check_graph_methods import *


def get_child_nodes(T, parent_node):
    bfs_edges = list(nx.bfs_edges(T, source=parent_node, depth_limit=1))
    child_nodes = [edge[1] for edge in bfs_edges if edge[1] > parent_node]
    if len(child_nodes) > 2:
        raise Exception("There may only be two child nodes, but there are following: " + str(child_nodes))
    return child_nodes


def get_parent_node(T, child_node):
    bfs_edges = list(nx.bfs_edges(T, source=child_node, depth_limit=1))
    parent_node = [edge[1] for edge in bfs_edges if edge[1] < child_node]
    if len(parent_node) != 1:
        raise Exception("There was not exactly one parent node found, but the following: " + str(parent_node))
    return parent_node[0]


def has_node_parent_node(T, potential_child_node):
    bfs_edges = list(nx.bfs_edges(T, source=potential_child_node, depth_limit=1))
    for edge in bfs_edges:
        if edge[1] < potential_child_node:
            return True
    return False


def get_root_node(T):
    root_node = None
    for node in T.nodes:
        if not has_node_parent_node(T, node):
            if root_node is None:
                root_node = node
            else:
                raise Exception("There may not be multiple root nodes", root_node, "and", node)
    return root_node


def get_unsat_sat_child(T, child_nodes):
    for id, child_node in enumerate(child_nodes):
        if T.nodes[child_node]["value"] == "UNSAT":
            return child_node, child_nodes[~id]
    return None, None


def get_normal_childs(T, child_nodes):
    normal_childs = []
    for id, child_node in enumerate(child_nodes):
        if T.nodes[child_node]["value"] != "UNSAT":
            normal_childs.append(child_node)
    return normal_childs


def get_unsat_path_to_final_sat_node(T, unsat_parent_node, unsat_path):
    child_nodes = get_child_nodes(T, unsat_parent_node)
    unsat_child, normal_node = get_unsat_sat_child(T, child_nodes)
    if unsat_child is not None and normal_node is not None:
        unsat_path.append(unsat_parent_node)
        unsat_path = get_unsat_path_to_final_sat_node(T, normal_node, unsat_path)
    return unsat_path


def get_root_to_sat_paths(T, paths=None, parent_node=0, path=None, func_is_sat_node=None):
    if paths is None:
        paths = []
    if path is None:
        path = []
    # This is a reference to a function to actually determine the "sat node"
    # It is different for the conversion of the final rule to an expression and
    # after that, because the last sat and unsat nodes are removed
    # That's why func_is_sat_node can be pointing to the function 'is_grand_child_sat_node' or 'is_child_sat_node'
    if func_is_sat_node is None:
        func_is_sat_node = is_grand_child_sat_node
    if len(get_child_nodes(T, parent_node)) > 0:
        if func_is_sat_node(T, parent_node):
            paths.append(path)
            return paths
        child_nodes = get_child_nodes(T, parent_node)
        normal_childs = get_normal_childs(T, child_nodes)
        for normal_child in normal_childs:
            paths = get_root_to_sat_paths(T, paths, normal_child, path + [(parent_node, normal_child)],
                                          func_is_sat_node)
    return paths


def get_final_sat_paths_for_region_plotting(T, root_node, paths=None, sat_nodes=None, path=None):
    """
    This method returns the final sat paths by recursively calling itself to search only paths with normal childs.
    For working correctly the initial parent_node must be the root node.
    This have to be set explicitly if compressing by removing UNSAT paths, because the root can change (i.e. not being
    0) if the original root node have an UNSAT child
    """
    if paths is None:
        paths = []
    if sat_nodes is None:
        sat_nodes = []
    if path is None:
        path = []
    # This is a reference to a function to actually determine the "sat node"
    # It is different for the conversion of the final rule to an expression and
    # after that, because the last sat and unsat nodes are removed
    # That's why func_is_sat_node can be pointing to the function 'is_grand_child_sat_node' or 'is_child_sat_node'

    if is_final_sat_node(T, root_node):
        paths.append(path)
        sat_nodes.append(root_node)
        return paths, sat_nodes

    child_nodes = get_child_nodes(T, root_node)
    if len(child_nodes) > 0:
        normal_childs = get_normal_childs(T, child_nodes)
        for normal_child in normal_childs:
            paths, sat_nodes = get_final_sat_paths_for_region_plotting(T, normal_child, paths, sat_nodes,
                                                                       path + [(root_node, normal_child)])
    return paths, sat_nodes


def get_final_sat_nodes_for_parent_of_sat_node(T, parent_sat_node, child_nodes=None):
    if child_nodes is None:
        child_nodes = get_child_nodes(T, parent_sat_node)
    final_sat_node = []
    for child_node in child_nodes:
        if is_final_sat_node(T, child_node):
            final_sat_node.append(child_node)
    if len(final_sat_node) == 0:
        raise Exception("No final_sat_node found for given parent of sat node " + str(parent_sat_node))
    return final_sat_node


def get_non_unsat_child(T, parent_of_unsat_node):
    child_nodes = get_child_nodes(T, parent_of_unsat_node)
    idx = 0
    if "UNSAT" in T.nodes[child_nodes[0]]["value"]:
        idx = ~idx
    return child_nodes[idx]


def get_unsat_path_assertions(T, unsat_path):
    unsat_path_assertions = []
    for path_id, node in enumerate(unsat_path):
        # assertion_term = T.nodes[node]["assert_term"]
        next_node_id = path_id + 1
        if next_node_id < len(unsat_path):
            edge = T.edges[(node, unsat_path[next_node_id])]
            unsat_path_assertions.append(edge["above_path_assertions"][-1])
    return unsat_path_assertions


def get_adjusted_rules_of_simplification_node(T, node):
    path_rules = []
    # Adjust the rule as an assertion (but not in SMT for sympy)
    if "simplified_rules" not in T.nodes[node].keys():
        raise Exception("No simplication node provided")
    simplified_rules = T.nodes[node]["simplified_rules"]
    for simplified_rule in simplified_rules:
        rule = str.replace(str(simplified_rule), "ToReal(0)", "0")
        path_rules.append(rule)
    return path_rules


def get_adjusted_rules(T, current_node, target_node):
    path_rules = []
    # Adjust the rule as an assertion (but not in SMT for sympy)
    if "simplified_rules" in T.nodes[current_node].keys():
        path_rules += get_adjusted_rules_of_simplification_node(T, current_node)
    else:
        normal_edge = T.edges[(current_node, target_node)]
        rule = T.nodes[current_node]["value"]
        if normal_edge["weight"] == 0:
            rule = str.replace(rule, ">", "<=")
        path_rules.append(rule)
    return path_rules


def get_rules_for_all_sat_paths(T, root_sat_paths):
    rules_for_all_sat_paths = []
    for root_sat_path in root_sat_paths:
        sat_path_rules = []
        for (current_node, target_node) in root_sat_path:
            edge_rules = get_adjusted_rules(T, current_node, target_node)
            sat_path_rules += edge_rules
        rules_for_all_sat_paths.append(sat_path_rules)
    return rules_for_all_sat_paths


def get_last_w_b_hat_bt(T, next_subgraph_root, m):
    # w_hat has to be saved for each path
    w_hat = T.nodes[next_subgraph_root]['last_w_hat']
    w_hat = torch.Tensor(w_hat)

    b_hat = T.nodes[next_subgraph_root]['last_b_hat']
    b_hat = torch.Tensor(b_hat)

    return w_hat, b_hat


def get_unsat_nodes(T):
    unsat_nodes = []
    dfs_edges = list(nx.dfs_edges(T, source=0))
    for current_edge in dfs_edges:
        current_node = current_edge[1]
        if "UNSAT" in T.nodes[current_node]['value']:
            unsat_nodes.append(current_node)
    return unsat_nodes


def get_sibling_node(T, node):
    parent_node = get_parent_node(T, node)
    sibling_node = get_child_nodes(T, parent_node)
    sibling_node.remove(node)
    return sibling_node[0]
