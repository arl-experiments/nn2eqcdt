from typing import Union, Type

import torch.nn as nn


def check_model(model):
    check_layer(model)
    check_bias(model)


def check_layer(model):
    """
    This method checks the given model. Only flat sequential models with [Linear, ReLU] blocks with or without a last
    activation function such as Linear or Identity or rather an activation such as ReLU or Tanh is assumed.
    The type chain the model has to follow therefore looks like the following:
    [Lin, (ReLU-Lin)+, (Nil|(Tanh, ReLU, Identity))]
    """
    type_chain_string = "expected [Lin, (((BatchNorm1d)[0..1]-(ReLU | LeakyReLU | PReLU)-Lin)+ , (Nil|(Tanh, ReLU, Identity))] type chain"

    cnt = 0

    relu_types = (nn.ReLU, nn.LeakyReLU, nn.PReLU)

    expect_next = nn.Linear
    # Modified from source: https://stackoverflow.com/questions/54203451/how-to-iterate-over-layers-in-pytorch
    for i, (name, m) in enumerate(model.named_children()):
        cnt += 1
        if isinstance(m, expect_next):
            if isinstance(m, nn.Linear):
                expect_next = (relu_types, nn.BatchNorm1d)
                # Lin-ReLU-Lin-(Tanh|ReLU|Identity)
                if i == len(model) - 2:
                    expect_next = (nn.Tanh, relu_types, nn.Identity)

            elif isinstance(m, relu_types):
                expect_next = nn.Linear
            elif isinstance(m, nn.BatchNorm1d):
                expect_next = relu_types
        else:
            raise Exception("Not follows, " + type_chain_string)
    if cnt < 3:
        raise Exception("Too few layers, " + type_chain_string)


def check_bias(model):
    """
    This method checks if either all linear layers of the given model have biases activated or not
    """
    bias = None
    for i, (name, m) in enumerate(model.named_children()):
        if isinstance(m, nn.Linear):
            tmp_bias = False if m.bias is None else True
            if bias is None:
                bias = tmp_bias
            else:
                if bias is not tmp_bias:
                    raise Exception("Not all lin layers have either bias True or False")
