import os
import pickle
from typing import Union
import networkx as nx

from src.models.graph_structures.NeighborGraph import NeighborGraph

from src.models.graph_structures import Tree


def get_output_dir():
    output_dir_path = os.path.join(os.path.dirname(__file__),
                                   '../../output')
    return output_dir_path


def join_output_path(filename: str):
    return os.path.join(get_output_dir(), filename)


def dump_tree(nx_tree: nx.Graph, tree_type: Union[type(Tree), type(NeighborGraph)], tree_path: str = None):
    if tree_path is None:
        class_name = tree_type.__name__
        tree_path = join_output_path(class_name + '.pickle')
    with open(tree_path, 'wb') as handle:
        pickle.dump(nx_tree, handle, protocol=pickle.HIGHEST_PROTOCOL)


def load_tree(tree_type: Union[type(Tree), type(NeighborGraph)], tree_path: str = None) -> nx.Graph:
    if tree_path is None:
        class_name = tree_type.__name__
        tree_path = join_output_path(class_name + '.pickle')
    with open(tree_path, 'rb') as handle:
        nx_tree = pickle.load(handle)
        return nx_tree
