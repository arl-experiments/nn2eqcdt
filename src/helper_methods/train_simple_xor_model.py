import os

import torch
from pytorch_lightning import loggers as pl_loggers
from pytorch_lightning.callbacks.progress import TQDMProgressBar
from torchmetrics.classification import BinaryAccuracy
from torchmetrics.classification import BinaryAUROC
import numpy as np

PATH_DATASETS = os.environ.get("PATH_DATASETS", ".")
BATCH_SIZE = 256 if torch.cuda.is_available() else 64

import torch
import pytorch_lightning as pl
from torch import nn, Tensor


class XorModel(pl.LightningModule):
    def __init__(self, learning_rate=0.1):
        super().__init__()
        self.model = torch.nn.Sequential(
            torch.nn.Linear(2, 2, bias=True),
            torch.nn.ReLU(),
            torch.nn.Linear(2, 1, bias=True)
        )

        # Set our init args as class attributes
        self.learning_rate = learning_rate

        self.loss_function = torch.nn.BCEWithLogitsLoss()  # torch.nn.CrossEntropyLoss()
        self.sigmoid = nn.Sigmoid()

        self.train_accuracy = BinaryAccuracy(threshold=0.5)
        self.val_accuracy = BinaryAccuracy(threshold=0.5)
        self.test_accuracy = BinaryAccuracy(threshold=0.5)

        self.auroc_func = BinaryAUROC()

        self.pred_labels = np.empty(0)
        self.y_labels = np.empty(0)

    def forward(self, src: Tensor) -> Tensor:
        """
        Args:
            src: Tensor, shape [seq_len, batch_size]
            src_mask: Tensor, shape [seq_len, seq_len]

        Returns:
            output Tensor of shape [seq_len, batch_size, ntoken]
        """
        output = self.model(src)
        return output

    def _step(self, batch):
        x, y = batch
        logits = self(x)

        loss = self.loss_function(logits, y.float())

        pred = self.sigmoid(logits)

        return logits, pred, y, loss

    def training_step(self, batch, batch_nb):
        logits, pred, y, loss = self._step(batch)

        # Calling self.log will surface up scalars for you in TensorBoard
        self.log("train_loss", loss, prog_bar=True)
        self.log("train_acc", self.train_accuracy(pred, y), prog_bar=True)

        auroc = self.auroc_func(pred, y)

        self.log("train_auroc", auroc)

        return loss

    def test_step(self, batch, batch_idx):
        logits, pred, y, loss = self._step(batch)

        pred_label = torch.clone(logits)
        func = (lambda x: 0 if x < 0 else 1)
        pred_label = pred_label.detach().cpu()
        pred_label.apply_(func)

        self.pred_labels = np.append(self.pred_labels, pred_label.cpu().numpy())
        self.y_labels = np.append(self.y_labels, y.cpu().numpy())

        # Calling self.log will surface up scalars for you in TensorBoard
        self.log("test_loss", loss, prog_bar=True)
        self.log("test_acc", self.train_accuracy(pred, y), prog_bar=True)

        return loss

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=self.learning_rate)


import torch.utils.data as data_utils

x = torch.Tensor([[0., 0.],
                  [0., 1.],
                  [1., 0.],
                  [1., 1.]])
y = torch.IntTensor([[0],
                     [1],
                     [1],
                     [0]])

train = data_utils.TensorDataset(x, y)
train_loader = data_utils.DataLoader(train, batch_size=1, shuffle=True)

# Train the model
model = XorModel()
trainer = pl.Trainer(
    accelerator="auto",
    devices=1 if torch.cuda.is_available() else None,  # limiting got iPython runs
    max_epochs=40,
    callbacks=[TQDMProgressBar(refresh_rate=1)],
    logger=pl_loggers.TensorBoardLogger(save_dir="tensorboard_logs/"),
)
trainer.fit(model, train_loader)

trainer.test(model, train_loader)

torch.save(model.model, 'xor_sample_model_1acc_with_bias')
