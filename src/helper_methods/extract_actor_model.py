import torch
import os

models_dir_path = os.path.join(os.path.dirname(__file__),
                               '../../models')

actor_path = os.path.join(models_dir_path, 'ddpg_actor_nn')

model = torch.load(actor_path)
print(model)