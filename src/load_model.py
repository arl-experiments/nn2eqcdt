import os
import torch


def load_model():
    model = None

    model_path = "../models/mountain_car_cont_ddpg_model_ac_pi_pi.pt"
    if os.path.isfile(model_path):
        model = torch.load(model_path)
    return model
