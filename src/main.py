import time

from src.models.PathCheckingOptions import PathCheckingOptions

from check_model import *
from plot_methods import *


def main():
    model_path = os.getenv('MODEL_PATH', '../models/smaller_ac_pi_pi_model_MCC_box.pt')
    model = torch.load(model_path, map_location=torch.device('cpu'))

    # model = nn.Sequential(
    #    nn.Linear(2,3, bias=True),
    #    nn.BatchNorm1d(3, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
    #    nn.ReLU(),
    #    nn.Linear(3,2, bias=True),
    # )

    # Set inference phase for BatchNorm to use statistics vars and not running
    # Source: https://stackoverflow.com/a/60018731
    model.eval()

    start = time.time()

    x_min, x_max = -1.2, 0.6
    y_min, y_max = -0.07, 0.07

    check_model(model)

    path_checking_options = PathCheckingOptions(model, [
        (x_min, x_max),
        (y_min, y_max)
    ])

    tree = Tree(path_checking_options)
    tree.build_tree(model)
    T = tree.get_T()

    tree.remove_unsat_paths()

    end = time.time()
    elapsed_time = end - start


    dump_tree(tree.get_plain_T(), Tree)

    # tree_loaded = load_tree(Tree)

    plot_with_pyvis(T)
    print("\nAmount of nodes:", str(len(T.nodes)))

    print("Building DT took:", str(elapsed_time))

    start = time.time()

    ng = NeighborGraph(tree, path_checking_options)
    ng.build_neighbor_graph()

    end = time.time()
    elapsed_time = end - start


    dump_tree(ng.get_plain_T(), NeighborGraph)
    # tree_loaded = load_tree(NeighborGraph)

    print("Building NG took:", str(elapsed_time))

    ng.plot_with_pyvis()

# plot_with_NetworkXDT2LaTeXForestDT(T, output_path)

# plot_regions(T, x_var_int=(x_min, x_max), y_var_int=(y_min, y_max))


if __name__ == '__main__':
    main()
