from typing import List

import torch
from z3 import *
from src.models.PathCheckingOptions import PathCheckingOptions
from src.models.graph_structures.Rule import Rule


def calc_rules(t: torch.Tensor, b: torch.Tensor, final=False) -> List[Rule]:
    """
    This calcs the rules and assertion terms from the given tensor, e. i.
    it treats the values as matrix coefficients and linearizes it
    The assertion terms are z3 assertions that get wrapped by <= or > according to its branch
    The rules get append with a >0 at default, if its not the final layer. There only the rule term itself is used.
    """
    rules = []
    format_specifier = '.8f'
    for i in range(t.shape[0]):
        coeffs = []
        for j in range(t.shape[1]):
            postpend_var = 'x' + str(j)
            #if j == 0:
            #    postpend_var = "x"
            #elif j == 1:
            #    postpend_var = "y"
            #else:
            #    raise Exception("not yet implemented other than exactly 2 output variables")

            b_i = b[i].detach().numpy()
            t_i_j = t[i][j].numpy()

            # These needs to be formatted because floats may be represented in scientific notation in
            # pytorch and numpy but cannot be processed in this by z3 reading from string
            b_i = format(b_i, format_specifier)
            t_i_j = format(t_i_j, format_specifier)

            rule_part = t_i_j + '*' + postpend_var

            if j == t.shape[1] - 1:
                rule_part += ' + ' + b_i
                if not final:
                    rule_part += '>0'
            else:
                rule_part += ' + '
            rule_string += (rule_part)
            coeffs.append(t_i_j)

            assert_term_part = '(* ' + t_i_j + ' ' + postpend_var + ') '
            assert_term += assert_term_part
            if j == t.shape[1] - 1:
                assert_term_part = '(* ' + b_i + ' 1) '
                assert_term += assert_term_part
        rules.append(Rule(rule_string, coeffs, assert_term))
    return rules


def check_path(path, path_checking_options: PathCheckingOptions = None):
    """
    This gets z3 assertions in SMT format as input and checks
    with declared const vars if
    these rules to try are satisfiable with the definition interval constraints
    of the input of the vars

    If it is not satisfiable the tree building is left at the UNSAT node for its potential subgraph
    """

    if path_checking_options is None:
        invariants = []
        disable_compression = False
    else:
        invariants = path_checking_options.invariants
        disable_compression = path_checking_options.disable_compression

    if disable_compression:
        return sat, "[COMPRESSION DISABLED]"

    model = None

    s = Solver()

    # This fixes very long checking times with floats and fixed values (bias)
    # Credits to NikolajBjorner
    # Source: https://github.com/Z3Prover/z3/issues/6631#issuecomment-1466723231
    set_option("rewriter.elim_to_real", True)

    for invariant in invariants:
        s.from_string(invariant)

    for rule in path:
        try:
            s.from_string(rule)
        except Exception as e:
            # One know issue is that z3 cannot parse scientific notation
            # (e.g. introduced by pytorch, therefore formatted explicitly in calc_rules_terms method above)
            raise Exception("The following rule could not be read by z3: " + str(rule) + " because\n" + str(e))
    t = s.check()
    if t == sat:
        model = s.model()
        print(model)
    else:
        print(t)
    return t, model


def simplify_assertions(assertion_terms):
    t = Then('solver-subsumption',
             'propagate-ineqs')

    g = Goal()

    smt2_string = '(declare-const x Real) (declare-const y Real)'
    for assertion_term in assertion_terms:
        smt2_string += assertion_term + " "

    g.add(parse_smt2_string(smt2_string))

    r = t(g)

    return r[0]

