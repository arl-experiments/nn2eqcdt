from src.graph_methods.access_graph_methods import *
from src.rule_methods import *
from src.calc_methods import *
from src.helper_methods.output import *
from sympy.plotting.plot import unset_show


def plot_regions(T, x_var_int, y_var_int):
    from sympy import plot_implicit, symbols, Eq, And, Or
    from sympy.parsing.sympy_parser import parse_expr
    import matplotlib

    final_sat_paths, sat_nodes = get_final_sat_paths_for_region_plotting(T, get_root_node(T))

    print("paths for region plotting", final_sat_paths)
    rules_for_all_sat_paths = get_rules_for_all_sat_paths(T, final_sat_paths)
    print("rules for all sat paths", rules_for_all_sat_paths)

    p = None

    colors = list(matplotlib.colors.get_named_colors_mapping().keys())

    x = symbols('x')
    y = symbols('y')

    for i, root_sat_path in enumerate(rules_for_all_sat_paths):
        str_expr = build_str_expr_from_path_rules(root_sat_path)
        p7 = plot_implicit(parse_expr(str_expr), x_var=(x, x_var_int[0], x_var_int[1]),
                           y_var=(y, y_var_int[0], y_var_int[1]), show=False, line_color=colors[i % len(colors)])
        if p is None:
            p = p7
        else:
            p.extend(p7)
    # This only process the series according to the implicit plotting, but not actually showing the plot with a backend
    unset_show()
    x, y = p.show()

    x_list, y_list, z_list, sat_node_expr_list = calc_data_points(T, x, y, sat_nodes)

    plot_surface_of_points(x_list, y_list, z_list, sat_node_expr_list)


def plot_surface_of_points(x_list, y_list, z_list, sat_node_expr_list, ap=None):
    # Source modified from: https://stackoverflow.com/questions/69625661/create-a-3d-surface-plot-in-plotly,
    # https://plotly.com/python/3d-surface-plots/
    import plotly.graph_objects as go
    import numpy as np
    from scipy.interpolate import griddata

    data = []

    for idx, (x, y, z, sat_node_expr) in enumerate(zip(x_list, y_list, z_list, sat_node_expr_list)):
        x = np.array(x)
        y = np.array(y)
        z = np.array(z)
        print(sat_node_expr)
        xi = np.linspace(x.min(), x.max(), 100)
        yi = np.linspace(y.min(), y.max(), 100)

        X, Y = np.meshgrid(xi, yi)

        Z = griddata((x, y), z, (X, Y), method='cubic')

        data.append(go.Surface(x=xi, y=yi, z=Z))

    if ap is not None:
        import matplotlib as mpl
        colors = mpl.colormaps['viridis'].colors

        x, y, z, output_node, sat_node_expr, r = ap['x'], ap['y'], ap['z'], ap['output_node'], ap['sat_node_expr'], ap[
            'r']
        norm = mpl.colors.Normalize(vmin=min(r), vmax=max(r))
        data.append(go.Scatter3d(
            x=x,
            y=y,
            z=z,
            mode='markers',
            marker=dict(
                color=norm(r),  # set color to an array/list of desired values
                colorscale='Viridis',  # choose a colorscale
                opacity=0.8
            ),
            hoverinfo='text',
            hovertext=["i: " + str(i) + "\nx: " + str(x) + "\ny: " + str(y) + "\noutput_node: " + str(
                output_node) + "\nsat_node_expr: " + str(sat_node_expr) for i, (x, y, output_node, sat_node_expr) in
                       enumerate(zip(x, y, output_node, sat_node_expr))]
        ))
    fig = go.Figure(data=data)
    fig.update_layout(
        scene=dict(
            zaxis=dict(nticks=4, range=[-3, 3], ),
        ),
        margin=dict(r=10, l=10, b=10, t=10))
    out_file = join_output_path('region_plot.html')
    fig.write_html(out_file)


def plot_with_pyvis(T):
    from pyvis.network import Network

    nt = Network('1000px', '1000px', layout=True)
    nt.set_options("""
    const options = {
      "layout": {
        "hierarchical": {
          "enabled": true,
          "sortMethod": "directed",
          "shakeTowards": "roots"
        }
      },
      "interaction": {
        "multiselect": true,
        "navigationButtons": true,
        "zoomSpeed": 1.5
      }
    }
    """)

    for node in list(T.nodes):
        label = T.nodes[node]['value']
        layer = T.nodes[node]['layer']
        nt.add_node(node, title=str(node), label=label, layer=layer, physics=False)

    for edge in T.edges.data():
        l, r, attrib = edge
        weight = attrib['weight']
        nt.add_edge(l, r, title=weight, physics=False)
    out_file = join_output_path('network.html')
    nt.write_html(out_file)

def plot_with_NetworkXDT2LaTeXForestDT(T, output_path):
    from NetworkXDT2LaTeXForestDT import convert_dt
    dt_converter = convert_dt.DTConverter(T, xhift=1)
    forest_dt_str = dt_converter.convert()
    print(forest_dt_str)

    convert_dt.generate_latex_document(forest_dt_str, output_path, rotate=True)
    convert_dt.compile_document(output_path)
