
def build_str_expr_from_rules(rules_for_all_sat_paths):
    str_expr = "Or("

    for root_sat_path in rules_for_all_sat_paths:
        str_expr += build_str_expr_from_path_rules(root_sat_path) + ", "
    str_expr += ")"
    return str_expr


def build_str_expr_from_path_rules(root_sat_path):
    and_expr = 'And('

    for sat_path_rule in root_sat_path:
        and_expr += sat_path_rule + ", "

    and_expr += ")"
    return and_expr