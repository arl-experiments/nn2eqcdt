import numpy as np


def gen_math_term(coeffs, bias, comp_mul_sym: bool = True) -> str:
    math_term = ''

    for i, coeff in enumerate(coeffs):
        var_str = 'x' + str(i)

        mul_str = ''
        if comp_mul_sym:
            mul_str = '*'

        math_term += str(coeff) + mul_str + var_str + ' + '

        if i == len(coeffs) - 1:
            math_term += str(bias)
    return math_term


class Expr:

    def __init__(self, coeffs, bias):
        self.coeffs = coeffs
        self.bias = bias
        self.value_str = gen_math_term(self.coeffs, self.bias, False)
        self.math_term = gen_math_term(self.coeffs, self.bias)
        # The coefficients of linear functions, describing (multi dim) planes are part of the normal vector
        # which is orthogonal to the plane. The -1 has to be added, because the coefficients are from x_i,
        # but the normal vector coefficients has be read from the zero parametric form:
        # sum(a_i*x_i) = z <=> sum(a_i*x_i) + -1*z = 0
        self.normal_vec = np.array(self.coeffs + [-1])

    def __str__(self):
        return self.value_str
