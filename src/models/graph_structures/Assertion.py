
from src.models.graph_structures import Rule


def _get_cmp_op(weight, is_neighbor_calc=False):
    if weight == 0:
        cmp_op = '<='
    else:
        if is_neighbor_calc:
            # For calculation of the neighboring graph,
            # the idea is to check on overlappings of possible neighboring
            # fractals, whereby its boundaries are described by the assertions.
            # Therefore, the borders have to be
            # overlapable by adding the line of the border by the equal sign to the planes
            cmp_op = '>='
        else:
            cmp_op = '>'
    return cmp_op


class Assertion:

    def __init__(self, rule: Rule, weight: int, is_neighbor_calc: bool = False):
        self.rule: Rule = rule
        self.weight: int = weight
        self.is_neighbor_calc: bool = is_neighbor_calc
        self._cmp_op: str = _get_cmp_op(weight, is_neighbor_calc)
        self.value: str = self._create_assertion()
        self.math_value: str = self._gen_math_term()

    def _create_assertion(self):
        return '(assert (' + self._cmp_op + ' ' + self.rule.assertion_term + ' 0))'

    def _gen_math_term(self):
        return self.rule.math_term + " " + self._cmp_op + ' 0'
