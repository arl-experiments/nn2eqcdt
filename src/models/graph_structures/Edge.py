from typing import Optional

from src.models.graph_structures import Assertion, Node


class Edge:
    def __init__(self,
                 from_node: Node,
                 to_node: Node,
                 assertion: Optional[Assertion] = None):
        self.from_node = from_node
        self.to_node = to_node
        self.value = (from_node, to_node)
        self.assertion: Assertion = assertion
