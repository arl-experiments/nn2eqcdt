from typing import List

from src.property_calc.distance_calc import calc_neighbor_distances

from src.models.graph_structures.Path import check_sat

from src.models.PathCheckingOptions import PathCheckingOptions
from src.models.graph_structures import Tree, Node, Assertion
import networkx as nx
from z3 import sat


def calc_path_assertions_for_neighbor_checking(node) -> List[Assertion]:
    assertions = []
    for edge in node.above_path.edges:
        assertion = Assertion(edge.assertion.rule, edge.assertion.weight, True)
        assertions.append(assertion)
    return assertions


class NeighborGraph:

    def __init__(self, tree: Tree, pathCheckingOptions: PathCheckingOptions):
        self.tree: Tree = tree
        self.pathCheckingOptions: PathCheckingOptions = pathCheckingOptions
        self.expr_nodes: List[Node] = tree.expr_nodes
        self._G = nx.Graph()

    def build_neighbor_graph(self):
        for i, node in enumerate(self.expr_nodes):
            node = self.expr_nodes[i]
            assertions1 = calc_path_assertions_for_neighbor_checking(node)
            if not self._G.has_node(node.node_id):
                self._G.add_node(node.node_id, node=node)
            for j in range(len(self.expr_nodes) - i - 1):
                node_to_check_idx = i + j + 1
                node_to_check = self.expr_nodes[node_to_check_idx]
                assertions2 = calc_path_assertions_for_neighbor_checking(node_to_check)
                if not self._G.has_node(node_to_check.node_id):
                    self._G.add_node(node_to_check.node_id, node=node_to_check)
                if self._are_nodes_neighbors(assertions1, assertions2):
                    cos_distances = calc_neighbor_distances(node, node_to_check)
                    self._G.add_edge(node.node_id, node_to_check.node_id, cos_distances=cos_distances)
        print(self._G)

    def _are_nodes_neighbors(self, assertions1: List[Assertion], assertions2: List[Assertion]) -> bool:
        assertions = assertions1 + assertions2
        check_sat_res = check_sat(assertions, self.pathCheckingOptions)
        if check_sat_res == sat:
            return True
        return False

    def plot_with_pyvis(self):
        from pyvis.network import Network
        from src.helper_methods.output import join_output_path

        nt = Network('1000px', '1000px', layout=True)
        nt.set_options("""
        const options = {
          "interaction": {
            "multiselect": true,
            "navigationButtons": true,
            "zoomSpeed": 1.5
          }
        }
        """)

        for node_id in list(self._G.nodes):
            node: Node = self._G.nodes[node_id]['node']

            nt.add_node(node_id, title=str(node), label=str(node_id), physics=False)

        for edge in self._G.edges.data():
            l, r, attrib = edge
            nt.add_edge(l, r, label="cos_distances: " + str(attrib['cos_distances']), physics=False)
        out_file = join_output_path('neighbor_graph.html')
        nt.write_html(out_file)

    def get_plain_T(self):
        T = nx.Graph()

        for node_id in list(self._G.nodes):
            node: Node = self._G.nodes[node_id]['node']

            T.add_node(node_id, value=str(node))

        for edge in self._G.edges.data():
            l, r, attrib = edge
            weight = attrib['cos_distances']
            T.add_edge(l, r, weight=weight)

        return T
