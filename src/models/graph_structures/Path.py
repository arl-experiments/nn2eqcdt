from typing import List, Optional, Union, Tuple, Type

from z3 import Solver, set_option, sat, CheckSatResult, ModelRef

from src.models.PathCheckingOptions import PathCheckingOptions
from src.models.graph_structures import Assertion, Node, Edge


def check_sat(assertions: List[Assertion], path_checking_options: Optional[PathCheckingOptions] = None) -> \
        CheckSatResult:
    """
    This gets z3 assertions in SMT format as input and checks
    with declared const vars if
    these rules to try are satisfiable with the definition interval constraints
    of the input of the vars

    If it is not satisfiable the tree building is left at the UNSAT node for its potential subgraph
    """
    if path_checking_options is None:
        invariant_assertions = []
        disable_compression = False
    else:
        invariant_assertions = path_checking_options.invariants
        disable_compression = path_checking_options.disable_compression

    check_sat_res: CheckSatResult = sat

    if disable_compression:
        sat_model = "[COMPRESSION DISABLED]"
        return check_sat_res

    s = Solver()

    # This fixes very long checking times with floats and fixed values (bias)
    # Credits to NikolajBjorner
    # Source: https://github.com/Z3Prover/z3/issues/6631#issuecomment-1466723231
    set_option("rewriter.elim_to_real", True)

    for invariant_assertion in invariant_assertions:
        s.from_string(invariant_assertion)

    for assertion in assertions:
        try:
            s.from_string(assertion.value)
        except Exception as e:
            # One know issue was e.g. that z3 cannot parse scientific notation
            # (it was defaultly introduced by pytorch and
            # solved by explicitly formatting numbers in decimal notation)
            raise Exception(
                "The following rule could not be read by z3: " + str(assertion.value) + " because\n" + str(e))
    check_sat_res = s.check()
    return check_sat_res


class Path:
    def __init__(self,
                 nodes: List['Node'],
                 assertions=None):
        if assertions is None:
            assertions = []
        self.nodes: List[Node] = nodes
        self.edges: List[Edge] = []

        self.assertions: List[Assertion] = assertions
        self.check_sat_res: Optional[CheckSatResult] = None

    def check_sat(self, path_checking_options: Optional[PathCheckingOptions] = None) -> None:
        self.check_sat_res = check_sat(self.assertions, path_checking_options)

    def get_weights(self) -> List[int]:
        return [assertion.weight for assertion in self.assertions]