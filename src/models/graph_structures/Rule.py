from typing import List, Tuple

import numpy as np

from src.models.graph_structures.Expr import gen_math_term


class Rule:
    FORMAT_SPECIFIER = '.8f'

    def __init__(self, coeffs: np.array, bias: float):
        self._coeffs: np.array = coeffs
        self._formatted_coeffs: List[str] = \
            [format(coeff, Rule.FORMAT_SPECIFIER) for coeff in self._coeffs]

        self._bias: float = bias
        self._formatted_bias: str = format(self._bias, Rule.FORMAT_SPECIFIER)

        self.assertion_term, self.assertion_term_parts = self._gen_assertion_term()
        self.value_str = gen_math_term(self._coeffs, self._bias, False) + " > 0"
        self.math_term = gen_math_term(self._coeffs, self._bias) + " > 0"

    def _gen_assertion_term(self) -> Tuple[str, List[str]]:
        assertion_term = ''
        assertion_term_parts = []

        for i, coeff in enumerate(self._formatted_coeffs):
            var_str = 'x' + str(i)

            assert_term_part = f'(* {coeff} {var_str}) '
            assertion_term_parts.append(assert_term_part)
            assertion_term += assert_term_part

            if i == len(self._formatted_coeffs) - 1:
                assert_term_part = f'(* {self._formatted_bias} 1) '
                assertion_term_parts.append(assert_term_part)
                assertion_term += assert_term_part
            else:
                assertion_term += ' '

        if len(self._formatted_coeffs) > 1:
            assertion_term = '(+ ' + assertion_term + ' )'

        return assertion_term, assertion_term_parts

    def __str__(self):
        return self.value_str
