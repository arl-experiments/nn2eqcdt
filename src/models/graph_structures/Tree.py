import math
import sys
import time
from typing import Deque

import torch.nn as nn
import torch
import networkx as nx
import copy

from src.models.graph_structures import *
from src.models.PathCheckingOptions import PathCheckingOptions


def get_lin_weight_bias(m: nn.Linear, i: int, mods: List):
    wt = m.weight.data
    bt = torch.zeros(wt.shape[0])
    if m.bias is not None:
        bt = m.bias.data

    if i + 1 < len(mods) and isinstance(mods[i + 1], nn.BatchNorm1d):
        bn: nn.BatchNorm1d = mods[i + 1]
        mult = bn.weight.data / torch.sqrt(bn.running_var + bn.eps)

        wt = torch.transpose(torch.transpose(wt, 0, 1) * mult, 0, 1)
        bt = (bt - bn.running_mean) * mult + bn.bias.data
    return wt, bt


def get_relu_neg_weight(i: int, mods: List) -> float:
    relu_neg_weight: float = 0
    if i - 1 > 0:
        mod = mods[i - 1]
        if isinstance(mod, nn.PReLU):
            prelu: nn.PReLU = mod
            assert len(prelu.weight.data) == 1  # Multiple channel are not supported yet for PReLU
            relu_neg_weight = float(prelu.weight.data.detach().numpy())
        if isinstance(mod, nn.LeakyReLU):
            lrelu: nn.LeakyReLU = mod
            relu_neg_weight = lrelu.negative_slope
    return relu_neg_weight


class Tree:

    def __init__(self, path_checking_options: PathCheckingOptions = None):
        self.path_checking_options: PathCheckingOptions = path_checking_options
        self.node_counter = 0
        self._T = None
        self._unsat_nodes: List[Node] = []
        self.expr_nodes: List[Node] = []

        self._node_counter = 0
        self._nodes_gen_velocity = None
        self._start_time = None

    def build_tree(self, model):
        self._T = nx.balanced_tree(2, 0)
        subgraphs: List[Subgraph] = []
        mods = [module for module in model.modules() if not isinstance(module, nn.Sequential)]

        # Modified from source: https://stackoverflow.com/questions/54203451/how-to-iterate-over-layers-in-pytorch
        for i, (name, m) in enumerate(model.named_children()):
            if isinstance(m, nn.Linear):
                wt, bt = get_lin_weight_bias(m, i, mods)

                if i == 0:
                    w_hat = wt
                    b_hat = bt

                    subgraph: Subgraph = Subgraph(w_hat, b_hat)
                    subgraph.relu_neg_weight = get_relu_neg_weight(i, mods)

                    subgraph.calc_rules()

                    values = subgraph.get_new_values_queue()
                    value = values.popleft()
                    # The root node has no nodes in paths above it
                    above_path = Path([])
                    node = Node(above_path, NodeKind.SAT, value)

                    subgraph.root_node = node

                    self._add_node(node, 0)

                    self._add_node_to(node, subgraph, values)
                    subgraph.calc_previous_subgraph_paths_for_leaf_nodes()

                    subgraphs.append(subgraph)

                else:
                    next_subgraphs = []
                    for subgraph in subgraphs:
                        for sat_node in subgraph.leaf_sat_nodes:

                            next_subgraph = Subgraph(subgraph.w_hat, subgraph.b_hat, sat_node, subgraph)
                            next_subgraph.relu_neg_weight = get_relu_neg_weight(i, mods)

                            next_subgraph.calc_new_hat(wt, bt)

                            if i == len(model) - 1:
                                next_subgraph.calc_expr()
                            else:
                                next_subgraph.calc_rules()
                            values: Deque[Value] = next_subgraph.get_new_values_queue()
                            value = values.popleft()
                            sat_node.value = value

                            self._add_node_to(sat_node, next_subgraph, values)
                            next_subgraph.calc_previous_subgraph_paths_for_leaf_nodes()

                            next_subgraphs.append(next_subgraph)
                    subgraphs = next_subgraphs
        for subgraph in subgraphs:
            for sat_node in subgraph.leaf_sat_nodes:
                sat_node.kind = NodeKind.EXPR
                self._set_expr_value(sat_node)
            self.expr_nodes.extend(subgraph.leaf_sat_nodes)

    def _add_node_to(self,
                     node: Node,
                     subgraph: Subgraph,
                     values: Deque[Value]):
        """
        This build a new decision subgraph down from the provided subgraph root
        with the rules and corresponding assertion_terms extracted from a w_hat of a matrix layer

        On a root node there are attached two nodes with respecting edges as the lower equal and higher splits
        of the assertions
        by the add_nodes_edges method.
        In each split the method calls itself recursively to add also these nodes on decision splits,
        if the respective path is valid,
        so if all above assertions are satisfiable with the input constraints
        by z3 solver in check_path called in add_nodes_edges method.

        # Assumption: The root MUST already be satisfied with above assertions and be created
        # with its respective rule and corresponding assertion_term
        # as well as above path assertions and weights
        # The node id has to be passed here, because it needs to be increased between two subgraphs of the same layer
        # within the loop over the next_subgraph_roots_a
        # There are two subgraphs created for each decision split for lower equal and higher in add_nodes_edges method.
        """

        values = copy.copy(values)
        if len(values) > 0:
            next_value: Value = values.popleft()
            for weight in range(2):

                next_node: Node = self._add_node_edge(node, weight, next_value)

                # Now, as a child was added to the node along the path, it gets is of RULE kind
                node.kind = NodeKind.RULE

                if next_node.kind == NodeKind.SAT:
                    self._add_node_to(next_node, subgraph, values)
                elif next_node.kind == NodeKind.UNSAT:
                    self._unsat_nodes.append(next_node)
                else:
                    raise Exception("Next node must either be of SAT or UNSAT kind")
        else:
            if node.kind == NodeKind.SAT:
                subgraph.leaf_sat_nodes.append(node)

    def _add_node_edge(self,
                       root_node: Node,
                       weight: int,
                       next_value: Value) -> Node:
        """
        This method adds a `next_node` and an edge to it,
        according to the assertion, determined by the weight,
        to the tree.

        Parameters
        ----------
        root_node : Node
                    The root node to which a new node should be added
                    according to the weight.
                    The kind of the new next node is determined by checking its SAT
                    with all assertions from its
                    `above_path` assertions (alone SAT) and a new
                    assertion according to its `rule`, but
                    determined by the `weight`.
        weight : int
                    The weight of the next edge from the `root_node` to the `next_node`
                    in the tree.
                    It describes how the rule of the `root_node` is evaluated by input.
        next_rule : Optional[Rule]
                    The rule of the new next node to be added
                    It is None for the leaf node to be able
                    to compute the next hat matrices
        Returns
        -------

        """

        # The DT is initialized with the root node containing the first "basic" rule
        # The root node has no nodes in paths above it

        rule: Rule = root_node.value
        assertion = Assertion(rule, weight)

        to_node_path = root_node.add_to_above_path(assertion)

        node = Node(to_node_path, NodeKind.UNKNOWN, next_value)
        to_node_path.check_sat(self.path_checking_options)
        node.set_sat_kind(to_node_path)

        self._add_node(node, root_node.layer + 1)

        edge = Edge(root_node, node, assertion)
        to_node_path.edges.append(edge)

        self._add_edge(root_node, node, edge)

        return node

    def get_parent_node_id(self, child_node) -> int:
        bfs_edges = list(nx.bfs_edges(self._T, source=child_node, depth_limit=1))
        parent_node = [edge[1] for edge in bfs_edges if edge[1] < child_node]
        if len(parent_node) != 1:
            raise Exception("There was not exactly one parent node found, but the following: " + str(parent_node))
        return parent_node[0]

    def get_child_nodes(self, parent_node) -> List[int]:
        bfs_edges = list(nx.bfs_edges(self._T, source=parent_node, depth_limit=1))
        child_nodes = [edge[1] for edge in bfs_edges if edge[1] > parent_node]
        if len(child_nodes) > 2:
            raise Exception("There may only be two child nodes, but there are following: " + str(child_nodes))
        return child_nodes

    def get_sibling_node_id(self, node) -> int:
        parent_node = self.get_parent_node_id(node)
        sibling_node = self.get_child_nodes(parent_node)
        sibling_node.remove(node)
        return sibling_node[0]

    def has_node_parent(self, potential_child_node) -> bool:
        bfs_edges = list(nx.bfs_edges(self._T, source=potential_child_node, depth_limit=1))
        for edge in bfs_edges:
            if edge[1] < potential_child_node:
                return True
        return False

    def calc_path_attrib_list(self, path: Path, attrib, nodes_idx=None, attrib_list=None):
        if attrib_list is None:
            attrib_list = []
        if nodes_idx is None:
            nodes_idx = 0
        nodes = path.nodes
        parent_node = nodes[nodes_idx]

        nodes_idx += 1
        next_node = nodes[nodes_idx]
        child_nodes = self.get_child_nodes(parent_node)
        if len(child_nodes) > 0:
            if nodes[nodes_idx] in child_nodes:
                edge = (parent_node, next_node)
                attrib_list.append(self._T.edges[edge][attrib])
                self.calc_path_attrib_list(path, attrib, nodes_idx, attrib_list)
            else:
                raise Exception("Nodes do not make up path in given tree")
        return attrib_list

    def get_T(self):
        return self._T

    def get_plain_T(self):
        T = nx.balanced_tree(2, 0)

        for node in list(self._T.nodes):
            value = self._T.nodes[node]['value']
            layer = self._T.nodes[node]['layer']
            T.add_node(node, value=value, layer=layer)

        for edge in self._T.edges.data():
            l, r, attrib = edge
            weight = attrib['weight']
            T.add_edge(l, r, weight=weight)
        return T

    def _get_assertions(self, edges: List[Edge]) -> List[Assertion]:
        assertions = []
        for edge in edges:
            tree_edge: Edge = self._T.edges[edge.value]['edge']
            assertions.append(tree_edge.assertion)
        return assertions

    def _add_node(self, node: Node, layer: int):
        self._print_node_status()
        value = node.value if node is not None else None
        self._T.add_node(self.node_counter, value=str(value), node=node)
        node.node_id = self.node_counter
        self.node_counter += 1
        if layer is not None:
            self._set_to_layer(node, layer)

    def _set_to_layer(self, node, layer):
        node.layer = layer
        self._T.nodes[node.node_id]['layer'] = layer

    def _set_expr_value(self, node):
        expr_list = []
        for expr in node.value:
            expr_list.append(expr.value_str)
        expr_value = expr_list
        if len(expr_list) == 1:
            expr_value = expr_list[0]
        self._T.nodes[node.node_id]['value'] = str(expr_value)

    def _set_on_child_layer_of(self, node: Node, root_node: Node):
        parent_layer = self._T.nodes[root_node.node_id]['layer']
        self._set_to_layer(node, parent_layer + 1)

    def _add_edge(self, root_node: Node, node: Node, edge: Edge):
        self._T.add_edge(root_node.node_id, node.node_id, edge=edge, weight=edge.assertion.weight)

    def _replace_edge_by_new(self,
                             node_id: int,
                             node_id_of_old_edge: int,
                             node_id_of_new_edge: int):
        old_edge: Edge = self._T.edges[(node_id, node_id_of_old_edge)]["edge"]
        self._T.remove_edge(node_id, node_id_of_old_edge)
        self._T.add_edge(node_id, node_id_of_new_edge, edge=old_edge, weight=old_edge.assertion.weight)

    def remove_unsat_paths(self):
        for unsat_node in self._unsat_nodes:
            unsat_node_id = unsat_node.node_id
            parent_node_id = self.get_parent_node_id(unsat_node_id)
            sibling_node_id = self.get_sibling_node_id(unsat_node_id)

            # Only reconnect parent_parent_node_id to non-UNSAT child and remove old connections,
            # if the parent_node_id is not the root node
            if self.has_node_parent(parent_node_id):
                parent_parent_node_id = self.get_parent_node_id(parent_node_id)
                self._replace_edge_by_new(parent_parent_node_id, parent_node_id, sibling_node_id)

            # This has to be executed if parent_node_id is root or not root node
            self._T.remove_edge(parent_node_id, unsat_node_id)
            self._T.remove_edge(parent_node_id, sibling_node_id)

            self._T.remove_nodes_from([parent_node_id, unsat_node_id])
        self._unsat_nodes = []

    def _print_node_status(self):
        mod_nodes = 100
        if self._node_counter % mod_nodes == 0:
            if self._nodes_gen_velocity is None:
                self._nodes_gen_velocity = -1
            else:
                self._end_time = time.time()
                elapsed_time = (self._end_time - self._start_time) / 60
                self._nodes_gen_velocity = math.floor(mod_nodes / elapsed_time)
            self._start_time = time.time()
        sys.stdout.write("\rCurrent node: {} ({} nodes/min)".format(self._node_counter, self._nodes_gen_velocity))
        sys.stdout.flush()
        self._node_counter += 1
