import copy
from enum import Enum
from typing import Optional, List
from z3 import sat

from src.models.graph_structures import Assertion, Value, Expr
from src.models.graph_structures.Path import Path


class NodeKind(Enum):
    """
    The idea of node kinds are, that a node may be initialized
    with an unknown state, but usually it is first checked if the path to it
    is satisfiable or not. If not, it will be set to the UNSAT kind.

    If all path assertions to the node are satisfiable,
    it is set temporarily to SAT. In the next subgraph adding, further path sat
    checks are performed.

    If there are further rules to be added along the path,
    the SAT node becomes a RULE node. If not, it becomes an EXPR node.
    """
    UNKNOWN = 0
    UNSAT = 1,
    SAT = 2,
    RULE = 3,
    EXPR = 4


class Node:

    def __init__(self,
                 above_path: Path = None,
                 kind: NodeKind = NodeKind.UNKNOWN,
                 value: Optional['Value'] = None):
        self.node_id: Optional[int] = None

        above_path.nodes.append(self)
        self.above_path: Path = above_path
        self.above_subgraph_path: Optional[Path] = None

        self.kind: NodeKind = kind
        self.value: Optional[Value] = value

        self.layer = None

    def check_kind(self):
        if (self.kind == NodeKind.RULE or self.kind == NodeKind.EXPR) and self.value is None:
            raise Exception("Node cannot be type RULE with no actual rule or expr set")
        # If a Node is SAT, it usually has a value, but while building
        # the leaf SAT nodes have 'None' as values, because it is later set
        # with the new values calculated

    def set_sat_kind(self, root_path: Path):
        if root_path.check_sat_res == sat:
            self.kind = NodeKind.SAT
        else:
            self.kind = NodeKind.UNSAT
            self.value = "UNSAT"
        self.check_kind()

    def set_value(self, value: Value):
        self.kind = NodeKind.RULE
        self.value = value
        self.check_kind()

    def add_to_above_path(self, assertion: Assertion) -> Path:
        # Copy assertions to not modify above_path
        path = Path(copy.copy(self.above_path.nodes))
        path.assertions = copy.copy(self.above_path.assertions) + [assertion]
        # Copy edges to not modify above_path
        path.edges = copy.copy(self.above_path.edges)

        return path

    def __str__(self):
        if self.kind == NodeKind.RULE:
            return self.value.value_str
        elif self.kind == NodeKind.EXPR:
            expr_str_list = []
            node_expr: List[Expr] = self.value
            for expr in node_expr:
                expr_str_list.append(expr.value_str)
            return str(expr_str_list)
        else:
            return str(self.value)

    def calc_subgraph_path(self, subgraph_root_node: 'Node'):
        idx = None
        for i, node in enumerate(self.above_path.nodes):
            if node.node_id == subgraph_root_node.node_id:
                idx = i
                break
        subgraph_path_nodes = self.above_path.nodes[idx:]
        self.above_subgraph_path = Path(subgraph_path_nodes)
        self.above_subgraph_path.edges = self.above_path.edges[idx:]
        self.above_subgraph_path.assertions = self.above_path.assertions[idx:]
