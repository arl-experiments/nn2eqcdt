from collections import deque
from typing import List, Optional, Deque, Type

import torch
import copy

from src.models.graph_structures import Rule, Node, NodeKind, Expr, Value


def _calc_values(t: torch.Tensor, b: torch.Tensor, value_type: Type[Value]) -> List[Value]:
    """
    This calcs the rules and assertion terms from the given tensor, e. i.
    it treats the values as matrix coefficients and linearizes it
    The assertion terms are z3 assertions that get wrapped by <= or > according to its branch
    The rules get append with a >0 at default, if its not the final layer. There only the rule term itself is used.
    """

    values = []
    for i in range(t.shape[0]):
        coeffs = t[i].detach().numpy()
        bias = float(b[i].detach().numpy())
        rule = value_type(coeffs, bias)
        values.append(rule)
    return values


def _calc_new_w_hat(w_hat, new_filter):
    """
    This calcs the new w_hat by multiplying the new filter
    with the current w_hat
    """

    new_w_hat = torch.matmul(new_filter, w_hat)
    if len(new_w_hat.shape) < 2:
        new_w_hat = new_w_hat.unsqueeze(dim=0)
    return new_w_hat


def _calc_new_b_hat(b_hat, new_filter, bt):
    new_b_hat = torch.matmul(new_filter, b_hat)
    new_b_hat += bt

    return new_b_hat


def _calc_new_filter(wt, a):
    """
    This repeats the activation matrix a, multiplies it
    element-wise to the weight matrix (hadamard product)
    This is called filter in the NN-DT paper
    It is applied (leftsided multiplied) to the w and b hats
    """
    assert a.shape[1] == wt.shape[1]
    a_expanded = a.expand(wt.shape)

    new_filter = torch.mul(wt, a_expanded.squeeze())
    return new_filter


class Subgraph:

    def __init__(self,
                 w_hat: torch.Tensor,
                 b_hat: torch.Tensor,
                 root_node: Optional[Node] = None,
                 previous_subgraph: Optional['Subgraph'] = None):
        self.w_hat: torch.Tensor = torch.clone(w_hat)
        self.b_hat: torch.Tensor = torch.clone(b_hat)
        self._values: Optional[Deque[Optional[Value]]] = None

        self.root_node: Optional[Node] = root_node
        self.previous_subgraph: Optional[Subgraph] = previous_subgraph

        self.relu_neg_weight: Optional[float] = None

        self.leaf_sat_nodes: List[Node] = []

    def check_root_node(self):
        if self.root_node != NodeKind.SAT:
            raise Exception("The root node of subgraph has to be SAT")
        self.root_node.check_kind()

    def _compute_a(self) -> torch.Tensor:
        a_arr = []

        for assertion in self.root_node.above_subgraph_path.assertions:
            weight = assertion.weight
            if weight == 0:
                a_arr.append(self.relu_neg_weight)
            else:
                a_arr.append(weight)

        return torch.Tensor([a_arr])

    def calc_new_hat(self, wt, bt) -> None:
        a = self._compute_a()
        new_filter = _calc_new_filter(wt, a)
        self.w_hat = _calc_new_w_hat(torch.clone(self.w_hat), new_filter)
        self.b_hat = _calc_new_b_hat(torch.clone(self.b_hat), new_filter, bt)

    def calc_rules(self):
        # None has to be added to the rules deque to be able to get
        # all weights for a SAT path including the last rule evaluation
        # These are required to calculate the next hat matrices and to carry on
        rules = _calc_values(self.w_hat, self.b_hat, Rule) + [None]
        self._values = deque(rules)

    def calc_expr(self):
        exprs = [_calc_values(self.w_hat, self.b_hat, Expr)]
        self._values = deque(exprs)

    def get_new_values_queue(self) -> Deque[Value]:
        values_queue = copy.copy(self._values)
        return values_queue

    def calc_previous_subgraph_paths_for_leaf_nodes(self):
        for leaf_sat_node in self.leaf_sat_nodes:
            leaf_sat_node.calc_subgraph_path(self.root_node)
