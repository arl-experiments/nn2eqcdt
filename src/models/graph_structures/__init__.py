from typing import Union, Optional, List

from .Expr import Expr
from .Rule import Rule

Value = Union[Optional[Rule], List[Expr]]
from .Assertion import Assertion
from .Edge import Edge
from .Node import Node, NodeKind
from .Path import Path

from .Subgraph import Subgraph
from .Tree import Tree