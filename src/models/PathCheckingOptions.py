from typing import List, Tuple

import torch


class PathCheckingOptions:

    def __init__(self,
                 model=None,
                 constraints=None,
                 generate_var_declaration_invariant=True,
                 disable_compression=False):
        self.disable_compression = disable_compression
        self.invariants = []
        self.input_dim = None
        if model is not None:
            self.extract_input_dim(model)
        if generate_var_declaration_invariant:
            self.generate_var_declaration_invariants()
        if constraints is not None:
            self.set_dim_constraints(constraints)

    def extract_input_dim(self, model):
        """
        This method extracts the input dimensions from the given model and sets it in this dataclass
        to be able to dynamically set the incrementing variables for external libs like
        z3, sympy and numexpr while path checking or calculations

        The input model is assumed to start with a linear layer, from which the input dim is read out
        as the input dim for the whole model
        """
        # Modified from source: https://stackoverflow.com/questions/54203451/how-to-iterate-over-layers-in-pytorch
        for i, (name, m) in enumerate(model.named_children()):
            if i == 0 and isinstance(m, torch.nn.Linear):
                self.input_dim = m.weight.shape[1]
                break

        if self.input_dim is None:
            raise Exception("Input dim could not be read from given model")

    def set_dim_constraints(self, constraints: List[Tuple]):
        if constraints is None:
            raise Exception("Constraints are None")
        if self.input_dim is None:
            raise Exception("Input dim is not set")
        if len(constraints) > self.input_dim:
            raise Exception("List of constraints exceeds input dimensions")

        for i in range(len(constraints)):
            if constraints[i] is not None and len(constraints[i]) == 2:
                (xi_min, xi_max) = constraints[i]
                new_invariants = [f'(assert (> x{i} {xi_min}))',
                                  f'(assert (< x{i} {xi_max}))']
                self.invariants.extend(new_invariants)

    def generate_var_declaration_invariants(self):
        if self.input_dim is None:
            raise Exception("Input dim not set")
        for i in range(self.input_dim):
            self.invariants.append(f'(declare-const x{i} Real)')
