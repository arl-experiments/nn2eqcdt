import os

from src.graph_methods.access_graph_methods import *
from src.graph_methods.check_graph_methods import *
from src.models.graph_structures import Node, NodeKind


def get_edge_weight_child_node_dict(T, parent_node):
    child_nodes = get_child_nodes(T, parent_node)
    edge_weight_child_node_dict = {}
    for child_node in child_nodes:
        edge_weight_child_node_dict[T.edges[(parent_node, child_node)]["weight"]] = child_node
    return edge_weight_child_node_dict

def get_output(T, in_arr, parent_node=0):
    """
    This method returns the output for the input values according to the DT T spans.
    For working correctly the initial parent_node must be the root node.
    This have to be set explicitly if compressing by removing UNSAT paths, because the root can change (i. e. not being
    0) if the original root node have an UNSAT child
    """
    import numexpr as ne
    edge_weight_child_node_dict = get_edge_weight_child_node_dict(T, parent_node)
    parent_node_value: Node = T.nodes[parent_node]["node"]
    if parent_node_value.kind == NodeKind.UNSAT:
        return "UNSAT"
    else:

        for i in range(len(in_arr)):
            set_modul_vars(f'x{i}', in_arr[i])
        #x, y = in_arr[0], in_arr[1]
        #if "simplified_rules" in T.nodes[parent_node].keys():
        #    adjusted_simplication_rules = get_adjusted_rules_of_simplification_node(T, parent_node)
        #    eval_parent = True
        #    for adjusted_simplification_rule in adjusted_simplication_rules:
        #        eval_parent = ne.evaluate(adjusted_simplification_rule)
        #        if not eval_parent:
        #            break
        #else:
        if parent_node_value.kind == NodeKind.RULE:
            eval_parent = ne.evaluate(parent_node_value.value.math_term)
            if eval_parent:
                eval_child_node = edge_weight_child_node_dict[1]
            else:
                eval_child_node = edge_weight_child_node_dict[0]
            return get_output(T, in_arr, eval_child_node)
        elif parent_node_value.kind == NodeKind.EXPR:
            eval_values = []
            for expr in parent_node_value.value:
                eval_values.append(ne.evaluate(expr.math_term))
            return eval_values
        else:
            raise Exception("Not valid node kind " + str(parent_node_value.kind))


def set_modul_vars(name, value):
    # Source: https://stackoverflow.com/a/2933481
    import sys
    this_module = sys.modules[__name__]
    setattr(this_module, name, value)
