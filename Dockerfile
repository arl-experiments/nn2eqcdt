# syntax=docker/dockerfile:experimental

FROM python:3.11.1-slim-buster

RUN pip3 install torch==1.13.0+cpu -f https://download.pytorch.org/whl/torch_stable.html

WORKDIR /app

COPY requirements.txt /app/requirements.txt
COPY sympy /app/sympy
# Source for docker caching: https://stackoverflow.com/a/57282479
RUN pip3 install -r requirements.txt

ADD . /app

WORKDIR /app/sympy
RUN python setup.py install

WORKDIR /app
RUN pip3 install -e .

WORKDIR /app/src
CMD ["../start_script.sh"]

