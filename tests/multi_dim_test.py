import functools
import unittest
import torch.nn as nn
import numpy as np
from src.plot_methods import plot_with_pyvis

from src.graph_methods.building_graph_methods import *
from src.output_methods import *
#from helper_methods import inference_test_helper

import os


class TestMultiDim(unittest.TestCase):

    def setUp(self) -> None:
        self.resource_path = os.path.join(os.path.dirname(__file__),
                                          'resources')
    def test_simple_multi_dim_lin_relu(self):

        #model_path = os.path.join(self.resource_path, 'smaller_ac_pi_pi_model_2_8_8_1.pt')
        #model = torch.load(model_path)

        t = torch.Tensor([
            [0, 0, 0],
            [0, 1, 0],
            [1, 0, 0],
            [1, 1, 0],
        ])

        model = nn.Sequential(
            nn.Linear(3,2),
            nn.ReLU(),
            nn.Linear(2,1)
        )

        #for i, (name, m) in enumerate(model.named_children()):
        #    if isinstance(m, nn.Linear):
        #        if i == 0:
        #            m.weight.data = torch.Tensor([[1, -1, 0], [-1, 1, 0]])
        #        if i == 2:
        #            m.weight.data = torch.Tensor([[1, 0]])
        # Source: https://stackoverflow.com/questions/32208359/is-there-a-multi-dimensional-version-of-arange-linspace-in-numpy
        grid = np.mgrid[-2:2.1:0.1, -2:2.1:0.1, -2:2.1:0.1,].reshape(3, -1).T

        t = torch.Tensor(grid)

        dim_constraints = [
            (-2, 2),
            (-2, 2),
            (-2, 2),
        ]

        path_checking_options = PathCheckingOptions(model, dim_constraints)

        custom_assert = functools.partial(self.assertAlmostEqual, delta=0.0001)
        self.inference_test_helper(model, path_checking_options, t, custom_assert)

    def inference_test_helper(self, model, path_checking_options, t, custom_assert):
        T = build_tree(model, path_checking_options)
        convert_final_rule_to_expr(T)
        plot_with_pyvis(T)
        model_out = model(t)
        for i in range(len(t)):
            input = list(t[i].detach().numpy())
            output = get_output(T, input)
            if output == "UNSAT":
                self.fail("Output is unsat for input " + str(input))
            eval_parent, parent_node, parent_value = output
            custom_assert(list(model_out[i].detach().numpy()), eval_parent)