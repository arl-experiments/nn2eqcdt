import unittest

from src.graph_methods.building_graph_methods import *


class TestModelCheckMethod(unittest.TestCase):

    def setUp(self) -> None:
        self.resource_path = os.path.join(os.path.dirname(__file__),
                                          'resources')

    def test_z3_value_parsing(self):
        model_path = os.path.join(self.resource_path, 'z3_value_parsing_test_next_w_hat.pt')
        next_w_hat = torch.load(model_path)

        model_path = os.path.join(self.resource_path, 'z3_value_parsing_test_next_b_hat.pt')
        next_b_hat = torch.load(model_path)

        rules, assertion_terms = calc_rules_terms(next_w_hat, next_b_hat, False)

        all_path_assertions = []

        for assertion_term in assertion_terms:
            current_path_assertion = create_assertion('>', assertion_term)
            all_path_assertions += [current_path_assertion]

        try:
            check_path_res, model = check_path(all_path_assertions)
        except Exception as e:
            self.fail("Model could not be processed. The error is\n" + str(e))
