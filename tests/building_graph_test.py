import unittest
import torch
import torch.nn as nn

from src.graph_methods.building_graph_methods import *
from src.graph_methods.access_graph_methods import *


class TestGraphBuilding(unittest.TestCase):

    def get_simple_xor_model(self):
        model = nn.Sequential(
            nn.Linear(2, 2, bias=False),
            nn.ReLU(),
            nn.Linear(2, 1, bias=False)
        )
        for layer_idx, (name, param) in enumerate(model.named_parameters()):
            if "weight" in name:
                if layer_idx == 0:
                    param.data = (torch.Tensor([[1, -1], [-1, 1]]))
                if layer_idx == 1:
                    param.data = (torch.Tensor([[1, 1]]))
            print(name, param)
        return model

    def test_minimal_xor(self):
        model = self.get_simple_xor_model()

        T = build_tree(model)
        bfs_edges = list(nx.bfs_edges(T, source=0, depth_limit=3))
        print(bfs_edges)
        if T.nodes[0]['value'] != '1.00000000*x + -1.00000000*y + 0.00000000>0':
            raise Exception("Root node not valid")
        node_values = ['-1.00000000*x + 1.00000000*y + 0.00000000>0',
                       '-1.00000000*x + 1.00000000*y + 0.00000000>0',
                       '0.00000000*x + 0.00000000*y + 0.00000000>0',
                       '-1.00000000*x + 1.00000000*y + 0.00000000>0',
                       '1.00000000*x + -1.00000000*y + 0.00000000>0',
                       'UNSAT',
                       'SAT', 'UNSAT', 'UNSAT', 'SAT',
                       'UNSAT', 'SAT']
        for i, edge in enumerate(bfs_edges):
            if node_values[i] == "SAT":
                self.assertIn("SAT", T.nodes[edge[1]]['value'])
                #if "SAT" not in T.nodes[edge[1]]['value']:
                #    raise Exception("Node " + str(edge[1]) + " not contains SAT")
            else:
                self.assertEqual(T.nodes[edge[1]]['value'], node_values[i])
                #elif T.nodes[edge[1]]['value'] != node_values[i]:
                #raise Exception("Node " + str(edge[1]) + " has value "
                #                + str(T.nodes[edge[1]]['value']) +
                #                " but expected " + str(node_values[i]))

    def test_convert_last(self):
        model = self.get_simple_xor_model()

        T = build_tree(model)

        convert_final_rule_to_expr(T)

        bfs_edges = list(nx.bfs_edges(T, source=0, depth_limit=3))
        print(bfs_edges)
        self.assertEqual(T.nodes[0]['value'], '1.00000000*x + -1.00000000*y + 0.00000000>0')
        #if T.nodes[0]['value'] != "1.0*x + -1.0*y>0":
        #    raise Exception("Root node not valid")
        node_values = ['-1.00000000*x + 1.00000000*y + 0.00000000>0',
                       '-1.00000000*x + 1.00000000*y + 0.00000000>0',
                       '0.00000000*x + 0.00000000*y + 0.00000000',
                       '-1.00000000*x + 1.00000000*y + 0.00000000',
                       '1.00000000*x + -1.00000000*y + 0.00000000', 'UNSAT']
        for i, edge in enumerate(bfs_edges):
            self.assertEqual(T.nodes[edge[1]]['value'], node_values[i])
            #if T.nodes[edge[1]]['value'] != node_values[i]:
            #    raise Exception("Node " + str(edge[1]) + " has value "
            #                    + str(T.nodes[edge[1]]['value']) +
            #                    " but expected " + str(node_values[i]))
