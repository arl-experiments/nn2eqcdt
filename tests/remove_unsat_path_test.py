import unittest
import torch
import torch.nn as nn
import numpy as np
import functools

from src.graph_methods.building_graph_methods import *
from src.output_methods import *
import os


class TestRemovalOfUnsatPath(unittest.TestCase):

    def setUp(self) -> None:
        self.resource_path = os.path.join(os.path.dirname(__file__),
                                          'resources')

    def test_removal_of_unsat_path(self):
        model_path = os.path.join(self.resource_path, 'unsat_path_removal.pt')
        model = torch.load(model_path)

        T = build_tree(model)

        convert_final_rule_to_expr(T)

        remove_unsat_paths(T)

        edge_list = [(1, 4), (15, 17), (15, 20), (22, 27)]

        for edge in edge_list:
            has_edge = T.has_edge(*edge)
            self.assertTrue(has_edge, "Graph has removed node " + str(edge))

        removed_node_list = [2, 3, 16, 18, 19, 21, 26, 28]

        for removed_node in removed_node_list:
            has_node = T.has_node(removed_node)
            self.assertFalse(has_node, "Graph has removed node " + str(removed_node))

    def test_small_ac_pi_pi_MCC_root_UNSAT_child_output(self):
        model_path = os.path.join(self.resource_path, 'smaller_ac_pi_pi_model_MCC_box_root_UNSAT_child.pt')
        model = torch.load(model_path)

        x_min, x_max = -1.2, 0.6
        y_min, y_max = -0.07, 0.07

        # Source: https://stackoverflow.com/questions/32208359/is-there-a-multi-dimensional-version-of-arange-linspace-in-numpy
        xy = np.mgrid[x_min:x_max:0.1, y_min:y_max:0.1].reshape(2, -1).T

        t = torch.Tensor(xy)

        custom_assert = functools.partial(self.assertAlmostEqual, delta=0.0001)
        self.inference_test_helper(model, t, custom_assert, x_min, x_max, y_min, y_max)

    def inference_test_helper(self, model, t, custom_assert, x_min, x_max, y_min, y_max):

        invariants = [
            f'(assert (> x {x_min}))',
            f'(assert (< x {x_max}))',
            f'(assert (> y {y_min}))',
            f'(assert (< y {y_max}))'
        ]

        T = build_tree(model, invariants)
        convert_final_rule_to_expr(T)
        remove_unsat_paths(T)

        model_out = model(t)
        for i in range(len(t)):
            input = list(t[i].detach().numpy())
            output = get_output(T, input, get_root_node(T))
            if output == "UNSAT":
                self.fail("Output is unsat for input " + str(input))
            eval_parent, parent_node, parent_value = output
            custom_assert(list(model_out[i].detach().numpy()), eval_parent)
