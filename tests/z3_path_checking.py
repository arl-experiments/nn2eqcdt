import unittest

from src.graph_methods.building_graph_methods import *


class TestModelCheckMethod(unittest.TestCase):

    def setUp(self) -> None:
        self.resource_path = os.path.join(os.path.dirname(__file__),
                                          'resources')

    def test_z3_float_bias_handling(self):
        import numexpr as ne
        s = [
            '-0.87400889*x + -6.14919424*y + -0.69771892>0 True',
            '-0.02404629*x + 0.07402976*y + -0.20004284>0 False',
            '0.03501872*x + -0.02265351*y + -0.59045595>0 False',
            '-0.92523456*x + 13.75889206*y + -0.50305665>0 False',
            '1.86018360*x + 9.69360638*y + 0.66599214>0 False',
            '-0.33947918*x + -0.06940891*y + -0.66138750>0 True',
            '2.65055776*x + -2.00811553*y + 0.77574313>0 False',
            '-0.07887774*x + -0.60970330*y + -0.69171220>0 False',
        ]

        x = []
        y = []
        c = []
        cmp = []

        for si in s:
            s_tmp = si.split('*x + ')
            x.append(s_tmp[0])
            si = s_tmp[1]

            s_tmp = si.split('*y + ')
            y.append(s_tmp[0])
            si = s_tmp[1]

            s_tmp = si.split('>0 ')
            c.append(s_tmp[0])
            si = s_tmp[1]

            cmp_op = '<='
            if si == 'True':
                cmp_op = '>'
            cmp.append(cmp_op)

        path = []

        in_x, in_y = -5.0, -0.4

        for x, y, c, cmp in zip(x, y, c, cmp):
            path.append(f'(assert ({cmp} (+ (* {x} x) (* {y} y) (* {c} 1) ) 0))')
            expr = f'{x}*{in_x} + {y}*{in_y} + {c}{cmp}0'
            print("Expression", expr)
            eval_output = ne.evaluate(expr)
            print("Eval", eval_output)

        # This fails when x and y declared as z3 int
        t, model = check_path(path)
        self.assertEqual(t, sat)
