import functools
import unittest

import torch
import torch.nn as nn
import numpy as np
from src.check_model import check_model

from src.models.PathCheckingOptions import PathCheckingOptions
from src.models.graph_structures import Tree
from src.output_methods import *
import os


class TestModelOutput(unittest.TestCase):

    def setUp(self) -> None:
        self.resource_path = os.path.join(os.path.dirname(__file__),
                                          'resources')

    #    def test_simple_network_output(self):
    #        # Source for file relative paths: https://stackoverflow.com/a/6629706
    #        model_path = os.path.join(self.resource_path, 'xor_sample_model_1acc')
    #        model = torch.load(model_path)
    #        pathCheckingOptions = PathCheckingOptions(model)
    #        tree = Tree(pathCheckingOptions)
    #        tree.build_tree(model)
    #
    #        # Source: https://stackoverflow.com/questions/32208359/is-there-a-multi-dimensional-version-of-arange-linspace-in-numpy
    #        xy = np.mgrid[-2:2.1:0.1, -2:2.1:0.1].reshape(2, -1).T
    #
    #        for input in xy:
    #            dt_out, parent_node, parent_value = get_output(T, input)
    #
    #            input = torch.Tensor(input)
    #            nn_out = model(input).detach().numpy()[0]
    #
    #            self.assertAlmostEqual(dt_out, nn_out, delta=0.0001)

    def test_simple_network_output_with_bias(self):
        model_path = os.path.join(self.resource_path, 'xor_sample_model_1acc_with_bias')
        model = torch.load(model_path)
        pathCheckingOptions = PathCheckingOptions(model)
        tree = Tree(pathCheckingOptions)
        tree.build_tree(model)

        # Source: https://stackoverflow.com/questions/32208359/is-there-a-multi-dimensional-version-of-arange-linspace-in-numpy
        xy = np.mgrid[-2:2.1:0.1, -2:2.1:0.1].reshape(2, -1).T

        T = tree.get_T()

        for input in xy:
            dt_out = get_output(T, input)

            input = torch.Tensor(input)
            nn_out = model(input).detach().numpy()[0]

            self.assertAlmostEqual(dt_out, nn_out, delta=0.0001)

    def test_multi_dim_network_output(self):
        model = nn.Sequential(
            nn.Linear(2, 2), nn.ReLU(),
            nn.Linear(2, 2)
        )
        pathCheckingOptions = PathCheckingOptions(model)
        tree = Tree(pathCheckingOptions)
        tree.build_tree(model)

        # Source: https://stackoverflow.com/questions/32208359/is-there-a-multi-dimensional-version-of-arange-linspace-in-numpy
        xy = np.mgrid[-2:2.1:0.1, -2:2.1:0.1].reshape(2, -1).T

        T = tree.get_T()

        for input in xy:
            dt_out = get_output(T, input)

            input = torch.Tensor(input)
            nn_out = model(input).detach().numpy()

            # Source: https://stackoverflow.com/a/12139899
            np.testing.assert_almost_equal(dt_out, nn_out, decimal=5, err_msg='', verbose=True)

    def test_lin_bn_merge(self):
        model = nn.Sequential(
            nn.Linear(2, 3, bias=True),
            nn.BatchNorm1d(3, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
            nn.ReLU(),
            nn.Linear(3, 1)
        )
        model.eval()

        ms = []

        l = nn.Linear(2, 3, bias=True)
        last_linear = None

        mods = [module for module in model.modules() if not isinstance(module, nn.Sequential)]

        for i, (name, m) in enumerate(model.named_children()):
            if i == 0 and isinstance(m, nn.Linear):
                l.weight = m.weight
                l.bias = m.bias
                ms.append(m)

                if isinstance(mods[i+1], nn.BatchNorm1d):
                    print(mods[i+1])

            elif i == 1 and isinstance(m, nn.BatchNorm1d):
                # y = (x-E[x])/(sqrt(Var[x]+eps)) * gamma + beta <=>
                # y = ((xW^T+b)-E[x])/(sqrt(Var[x]+eps)) * gamma + beta <=>
                # mult = gamma/(sqrt(Var[x]+eps))
                # y = xW^T*mult+(b-E[x])*mult + beta <=>

                ms.append(m)

                bn = m

                mult = bn.weight / torch.sqrt(bn.running_var + bn.eps)

                l.weight = nn.Parameter(
                    torch.transpose(torch.transpose(l.weight, 0, 1) * mult,0, 1)
                )
                l.bias = nn.Parameter(
                    (l.bias - bn.running_mean) * mult + bn.bias
                )


                #mult = torch.divide(m.weight, torch.sqrt(m.running_var + m.eps))
                #l_w_T = torch.transpose(l.weight, 0, 1)
                #expanded_mult = mult.expand(l_w_T.shape)
                #w_mult = torch.mul(l_w_T, expanded_mult)
                #w_mult_T = torch.transpose(w_mult, 0, 1)
                #l.weight = nn.Parameter(w_mult_T)
#
                #mean_applied = l.bias - m.running_mean
                #expanded_mult = mult.expand(mean_applied.shape)
                #new_bias = torch.mul(expanded_mult, mult) + m.bias
                #l.bias = nn.Parameter(new_bias)
            elif i == 3:
                last_linear = m
        # Source: https://stackoverflow.com/questions/32208359/is-there-a-multi-dimensional-version-of-arange-linspace-in-numpy
        xy = np.mgrid[-2:2.1:0.1, -2:2.1:0.1].reshape(2, -1).T

        for input in xy:
            input = torch.Tensor([input])

#            lin: nn.Linear = ms[0]
#
#            out = ms[0](input)
#
#
#            out_l1 = torch.matmul(input, torch.transpose(lin.weight, 0, 1)) + lin.bias
#
#            out1 = ms[1](out)
#
#            bn: nn.BatchNorm1d = ms[1]
#
#            out2 = (out - bn.running_mean) / torch.sqrt(bn.running_var + bn.eps) * bn.weight + bn.bias
#
#            out2_t = (torch.matmul(input, torch.transpose(lin.weight, 0, 1)) + lin.bias - bn.running_mean) / torch.sqrt(bn.running_var + bn.eps) * bn.weight + bn.bias
#
#            lin1 = nn.Linear(2,3, bias=True)
#            lin1.weight = nn.Parameter(torch.transpose(torch.transpose(lin.weight, 0, 1) / torch.sqrt(bn.running_var + bn.eps) * bn.weight, 0, 1))
#            lin1.bias = nn.Parameter((lin.bias - bn.running_mean) / torch.sqrt(bn.running_var + bn.eps) * bn.weight + bn.bias)
#
#            out2_t1 = (torch.matmul(input, torch.transpose(lin.weight, 0, 1)) + lin.bias - bn.running_mean) / torch.sqrt(bn.running_var + bn.eps) * bn.weight + bn.bias
#            out2_t2 = lin1(input)


            nn_out = model(input).detach().numpy()
            nn_out = nn_out[0]
            nn2_out = l(input)
            nn2_out = nn.ReLU()(nn2_out)
            nn2_out = last_linear(nn2_out).detach().numpy()[0]

            self.assertAlmostEqual(nn_out, nn2_out, delta=0.0001)


    def test_multiple_layer_model_with_relu(self):
        model = nn.Sequential(
            nn.Linear(2, 3, bias=True),
            nn.ReLU(),
            nn.Linear(3, 3, bias=True),
            nn.ReLU(),
            nn.Linear(3, 1, bias=True)
        )
        model.eval()

        check_model(model)

        pathCheckingOptions = PathCheckingOptions(model)
        tree = Tree(pathCheckingOptions)
        tree.build_tree(model)

        # Source: https://stackoverflow.com/questions/32208359/is-there-a-multi-dimensional-version-of-arange-linspace-in-numpy
        xy = np.mgrid[-2:2.1:0.1, -2:2.1:0.1].reshape(2, -1).T

        T = tree.get_T()

        for input in xy:
            dt_out = get_output(T, input)

            input = torch.Tensor(input)
            nn_out = model(input).detach().numpy()[0]

            self.assertAlmostEqual(dt_out, nn_out, delta=0.0001)



    def test_multiple_layer_model_with_prelu(self):
        model = nn.Sequential(
            nn.Linear(2, 3, bias=True),
            nn.PReLU(),
            nn.Linear(3, 3, bias=True),
            nn.ReLU(),
            nn.Linear(3, 1, bias=True)
        )
        model.eval()

        check_model(model)

        pathCheckingOptions = PathCheckingOptions(model)
        tree = Tree(pathCheckingOptions)
        tree.build_tree(model)

        # Source: https://stackoverflow.com/questions/32208359/is-there-a-multi-dimensional-version-of-arange-linspace-in-numpy
        xy = np.mgrid[-2:2.1:0.1, -2:2.1:0.1].reshape(2, -1).T

        T = tree.get_T()

        for input in xy:
            dt_out = get_output(T, input)

            input = torch.Tensor(input)
            nn_out = model(input).detach().numpy()[0]

            self.assertAlmostEqual(dt_out, nn_out, delta=0.0001)


#    def test_small_ac_pi_pi_output(self):
#        model_path = os.path.join(self.resource_path, 'smaller_ac_pi_pi_model_2_8_8_1.pt')
#        model = torch.load(model_path)
#
#        # Source: https://stackoverflow.com/questions/32208359/is-there-a-multi-dimensional-version-of-arange-linspace-in-numpy
#        xy = np.mgrid[-5:5.1:0.1, -5:5.1:0.1].reshape(2, -1).T
#
#        t = torch.Tensor(xy)
#
#        custom_assert = functools.partial(self.assertAlmostEqual, delta=0.0001)
#        self.inference_test_helper(model, t, custom_assert)
#
#    def test_dt_small_ac_pi_pi_output(self):
#        model_path = os.path.join(self.resource_path, 'smaller_ac_pi_pi_model_2_8_8_1.pt')
#        model = torch.load(model_path)
#
#        # Source: https://stackoverflow.com/questions/32208359/is-there-a-multi-dimensional-version-of-arange-linspace-in-numpy
#        xy = np.mgrid[-5:5.1:0.1, -5:5.1:0.1].reshape(2, -1).T
#
#        for input in xy:
#
#            input = torch.Tensor(input)
#            nn_out = model(input).detach().numpy()[0]
#            tmp_output = input
#            tmp_output2 = input
#            w_hat, b_hat, a = None, None, None
#            for i, (name, m) in enumerate(model.named_children()):
#                tmp_output = m(tmp_output)
#                if isinstance(m, nn.Linear):
#                    tmp_output2 = torch.matmul(tmp_output2, torch.transpose(m.weight.data, 0, 1)) + m.bias.data
#                    if a is not None:
#                        # a = a.unsqueeze(dim=1)
#                        # a = a.expand(m.weight.data.shape)
#                        w_hat_a = torch.mul(m.weight.data, a)
#                        w_hat = torch.matmul(w_hat_a, w_hat)
#
#                        b_hat = torch.matmul(w_hat_a, b_hat) + m.bias.data
#
#                    if i == 0:
#                        w_hat = m.weight.data
#                        b_hat = m.bias.data
#
#                    a = torch.matmul(w_hat, input) + b_hat
#                    a.apply_(lambda x: x > 0)
#                else:
#                    tmp_output2 = m(tmp_output2)
#            out_hat = torch.matmul(w_hat, input) + b_hat
#
#            self.assertAlmostEqual(tmp_output, out_hat, delta=0.0001)
#            self.assertAlmostEqual(tmp_output, nn_out, delta=0.0001)
#            self.assertAlmostEqual(tmp_output, tmp_output2, delta=0.0001)
#
#    def test_simple_xor_bias_output(self):
#        t = torch.Tensor([
#            [0, 0],
#            [0, 1],
#            [1, 0],
#            [1, 1],
#        ])
#
#        model = nn.Sequential(
#            nn.Linear(2, 2, bias=True),
#            nn.ReLU(),
#            nn.Linear(2, 2, bias=True),
#            nn.ReLU(),
#            nn.Linear(2, 1, bias=True)
#        )
#
#        for i, (name, m) in enumerate(model.named_children()):
#            if isinstance(m, nn.Linear):
#                if i == 0:
#                    m.weight.data = torch.Tensor([[1, -1], [-1, 1]])
#                    m.bias.data = torch.Tensor([1, 0])
#                if i == 2:
#                    m.weight.data = torch.Tensor([[1, 0], [0, 1]])
#                    m.bias.data = torch.Tensor([-1, 0])
#                if i == 4:
#                    m.weight.data = torch.Tensor([[1, 1]])
#                    m.bias.data = torch.Tensor([0])
#        custom_assert = functools.partial(self.assertEqual)
#        # custom_assert = functools.partial(self.assertAlmostEqual, delta=0.0001)
#        self.inference_test_helper(model, t, custom_assert)
#
#    def inference_test_helper(self, model, t, custom_assert):
#        T = build_tree(model)
#        convert_final_rule_to_expr(T)
#        model_out = model(t)
#        for i in range(len(t)):
#            input = list(t[i].detach().numpy())
#            output = get_output(T, input)
#            if output == "UNSAT":
#                self.fail("Output is unsat for input " + str(input))
#            eval_parent, parent_node, parent_value = output
#            custom_assert(list(model_out[i].detach().numpy()), eval_parent)
