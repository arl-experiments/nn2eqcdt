import unittest

from src.check_model import *


class TestModelCheckMethod(unittest.TestCase):

    def test_positive(self):
        model = nn.Sequential(
            nn.Linear(2, 2, bias=False),
            nn.ReLU(),
            nn.Linear(2, 1, bias=False)
        )

        try:
            check_model(model)
        except Exception:
            self.fail("Model could not be verified")

    def test_non_inner_relu(self):
        model = nn.Sequential(
            nn.Linear(2, 2, bias=False),
            nn.Linear(2, 1, bias=False),
        )
        self.assertRaises(Exception, check_model, model)

    def test_multiple_lin_relu(self):
        model = nn.Sequential(
            nn.Linear(2, 2, bias=False),
            nn.ReLU(),
            nn.Linear(2, 2, bias=False),
            nn.Linear(2, 1, bias=False),
        )
        self.assertRaises(Exception, check_model, model)

    def test_multiple_relu_lin(self):
        model = nn.Sequential(
            nn.Linear(2, 2, bias=False),
            nn.ReLU(),
            nn.ReLU(),
            nn.Linear(2, 2, bias=False),
            nn.ReLU(),
            nn.Linear(2, 1, bias=False),
        )
        self.assertRaises(Exception, check_model, model)

    def test_only_relu(self):
        model = nn.Sequential(
            nn.ReLU()
        )
        self.assertRaises(Exception, check_model, model)

    def test_only_lin(self):
        model = nn.Sequential(
            nn.Linear(2, 2, bias=False)
        )
        self.assertRaises(Exception, check_model, model)

    def test_lin_relu(self):
        model = nn.Sequential(
            nn.Linear(2, 2, bias=False),
            nn.ReLU()
        )
        self.assertRaises(Exception, check_model, model)

    def test_first_relu(self):
        model = nn.Sequential(
            nn.ReLU(),
            nn.Linear(2, 2, bias=False)
        )
        self.assertRaises(Exception, check_model, model)
