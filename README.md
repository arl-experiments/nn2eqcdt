# Overview
This project transforms a simple MLP model (sequential linear-relu) into a compressed decision tree and 3D region plot.
It is tested with a model of a actor agent learned using Deep Reinforcement Learning (DRL) methods (DDPG) in order to
be able to improve the explainability of DRL learned agents.

## Features
- Transformation of PyTorch Linear-ReLU models into networkx DTs
  - Support for BatchNorm1d layer
  - Support for PReLU and LeakyReLU
- Building of NeighborGraph from DT
- OOP Tree structures (Tree, Node, Path, Edge, )
- Plotting DTs with LaTeX forest by NetworkXDT2LaTeXForestDT 

## Cloning this repo

This repo contains multiple submodules, so use the following to init them while cloning:
```sh
git clone --recurse-submodules https://gitlab.com/arl-experiments/nn2eqcdt.git
```

## Installation
### Prerequisites
- Linux/GNU (Tested with Ubuntu 20.04)
- Docker (with BuildKit) (tested with version 20.10.14, build a224086)
- Docker compose (tested with version docker-compose version 1.25.5, build 8a1c60f6)
- Webbrowser (tested with Firefox version 108.0.2)

### Docker compose
Build and run it with docker compose (also with user permission for output):

```sh
COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose build
UID_GID="$(id -u):$(id -g)" docker-compose up -d
```

Watch the logs with

```sh
watch docker-compose logs -t -f --tail 3
```

The output decision tree and 3D plot files are standalone and can therefore be opened with:
```sh
cd output
firefox network.html
firefox region_plot.html
```


### Tests
Setup a virtualenv and install the `requirements.txt`

Run tests with

```sh
python -m unittest tests/*_test.py
```

## Processing
The application loads a sequential linear-relu pytorch model defined by `MODEL_PATH` env in `docker-compose.yml` file
written by `torch.save`.
The model is then transformed into a decision tree that is saved as a html in output by pyvis.
After that the different input regions are determined by sympys `plot_implicit` and
computed a `z` valued based on the decision tree output. This `x-y-z` points
are than saved as a scatter in html plot format by plotly.

**NOTE** The current implementation only works for sequential linear-relu models,
with two input and one output dimension


## Models
The model `models/mountain_car_cont_ddpg_model_ac_pi_pi.pt` is the actor model, trained with ddpg,
on MountainCar continuous environment from OpenAI gym.

The model `models/smaller_ac_pi_pi_model_MCC_box.pt` was trained with MSE loss
and a sample of a small region from the model above.


The following gifs are demos for the trained MountainCarContinuous-v0 controller,
transformed decision tree and 3d plot of the distilled smaller model.

![Car drive of trained model in MountainCarContinuous-v0](models/mcc_model_car.gif)

![3D Region plot of the smaller actor model](models/smaller_ac_pi_pi_model_MCC_box_3dplot.gif)

![Decision tree of the smaller actor model](models/smaller_ac_pi_pi_model_MCC_box_dt.gif)
