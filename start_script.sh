#!/bin/bash
python main.py
# check for non-null/non-zero string variable
# Source: https://stackoverflow.com/a/3601734
if [ -n "$UID_GID" ]; then
  chown -R "${UID_GID}" ../output
fi
